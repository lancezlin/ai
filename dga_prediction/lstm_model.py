import pandas as pd
from keras.models import Sequential
import numpy as np
from keras.preprocessing import sequence
from keras.layers.core import Dense, Dropout, Activation
from keras.layers.embeddings import Embedding
from keras.layers.recurrent import LSTM
from keras.callbacks import ModelCheckpoint
import sklearn
from sklearn.cross_validation import train_test_split
import os
import yaml
import json


# get encoding model
def get_encoding_model(domains, to_file, path='models'):
    """"""
    maps = {}
    combo = set(''.join(domains))
    for idx, x in enumerate(combo):
        maps[x] = idx + 1
    model_path = os.path.join(path, to_file)
    create_yaml_file(model_path, maps)
    return maps

# get valid chars
def valid_chars(ds):
    """"""
    res = {}
    for ids, x in enumerate(set(''.join(ds))):
        res[x] = idx + 1
    return res

# create yaml file
def create_yaml_file(filename, inputs):
    """"""
    if not isinstance(inputs, dict):
        raise TypeError("Input is not a valid type")
        return
    output_file = filename + ".yaml"
    with open(output_file, 'w') as output:
        yaml.dump(inputs, output, default_flow_style=False)

    return 


# preprocess data
def preprocess(domains):
    """"""
    vchars = get_encoding_model(domains, 'chars_map')
    max_features = len(vchars) + 1
    max_length = np.max([len(domain) for domain in domains])
    X = [[vchars[y] for y in domain] for domain in domains]
    X = sequence.pad_sequences(X, maxlen=max_length, padding='post')
    X = np.array(X)
    return X, max_features, max_length

# save model to local
def save_keras_to_local(model, name, model_dir='models'):
    """"""
    weight_path = os.path.join(model_dir, name+'.h5')
    model_path = os.path.join(model_dir, name+'.json')

    try:
        model.save_weights(weight_path)
        with open(model_path, 'w') as f:
            f.write(model.to_json())
    except Exception as e:
        print ("Could not write model - {}".format(e))

# load model from local
def load_keras_from_local(name, model_dir='models'):
    """"""
    weigth_path = os.path.join(model_dir, name+'.h5')
    model_ptah = os.path.join(model_dir, name+'.json')
    try:
        with open(model_path, 'r') as r:
            model = model_from_json(r.read())
        model.load_weights(weight_path)
    except Exception as e:
        print ("Could not load model from local - {}".format(e.message))
        return None
    return model

# build lstm model
def build_lstm(max_feature, max_length):
    model = Sequential()
    model.add(Embedding(max_feature, 128, input_length=max_length))
    model.add(Dropout(0.3))
    model.add(LSTM(64, return_sequences=True))
    model.add(LSTM(64, return_sequences=True))
    model.add(LSTM(32, return_sequences=False))
    model.add(Dropout(0.5))
    model.add(Dense(1))
    model.add(Activation('sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer='adam')
    return model


if __name__=='__main__':
    data = pd.read_csv('all_domain.csv', index_col=0)
    model_path = '/home/lancel/Projects/ai/dga_prediction/models'
    data['domain'].fillna('', inplace=True)
    data_seq = data['domain'].tolist()
    y = data['class'].values

    X, max_feature, max_length = preprocess(data_seq)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

    model = build_lstm(max_feature, max_length)
    # set check pointers to save models
    checkpointer = ModelCheckpoint(filepath=model_path + '/model-{epoch:02d}.hdf5', verbose=1)
    # training the model
    model.fit(X_train, y_train, batch_size=10, epochs=3, callbacks=[checkpointer])
    # predict the results
    y_test_prob = model.predict_proba(X_test)

    # save model to local
    save_keras_to_local(model, 'lstm')

    # validate performance
    thresholds = np.linspace(0.1, 0.95, 18)
    pred_probas = model.predict_proba(X_test)
    #print (pred_probas)
    for thres in thresholds:
        y_test_pred_i = (pred_probas > thres).astype(int)
        cm = sklearn.metrics.confusion_matrix(y_test, y_test_pred_i)
        #print(cm)
        [[tp, fp], [_, _]] = cm
        acc_score = sklearn.metrics.accuracy_score(y_test, y_test_pred_i)
        print("FP: {0} vs ACC: {1} - threshold: {2}".format(1.0*fp/(fp+tp), acc_score, thres))
