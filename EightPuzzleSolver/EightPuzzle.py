import sys
import argparse
import time
import math
import resource

class Node(object):
	"""docstring for Node"""
	def __init__(self, state, parent, move, depth, cost):
		#super(Node, self).__init__()
		self.state = state
		self.parent = parent
		self.move = move
		self.depth = depth
		self.cost = cost

def swap(state, pos1, pos2):
	# swap two positions
	tmp = state[pos1]
	state[pos1] = state[pos2]
	state[pos2] = tmp
	return state

def position_by_value(node, value):
	cells = int(math.sqrt(len(node)))
	row_index = node.index(value) / cells
	col_index = node.index(value) % cells
	return row_index, col_index

def h(state, goal_state):
	# heuristic function 
	cost = 0
	for i in state:
		if i == 0: continue
		row_index, col_index = position_by_value(state, i)
		row_goal_index, col_goal_index = position_by_value(goal_state, i)
		cost += abs(row_index - row_goal_index) + abs(col_index - col_goal_index)
	return cost

def compare(x, y):
	# compare function
	return ((x.depth + h(x.state, goal_state)) - (y.depth + h(y.state, goal_state)))



class EightPuzzles(object):
	"""docstring for EightPuzzles"""
	def __init__(self):
		super(EightPuzzles, self).__init__()

	def Node_Create(self, state, parent, move, depth, cost):
		# create a node
		return Node(state, parent, move, depth, cost)


	def Right(self, state):
		# moving right
		next_state = state[:]
		position = next_state.index(0)
		if position not in [2, 5, 8]:
			return swap(next_state, position, position + 1)
		else:
			return None

	def Left(self, state):
		# moving left
		next_state = state[:]
		position = next_state.index(0)
		if position not in [0, 3, 6]:
			return swap(next_state, position, position - 1)
		else:
			return None

	def Up(self, state):
		# moving up
		next_state = state[:]
		position = next_state.index(0)
		if position not in [0, 1, 2]:
			return swap(next_state, position, position - 3)
		else:
			return None
			
	def Down(self, state):
		# moving down
		next_state = state[:]
		position = next_state.index(0)
		if position not in [6, 7, 8]:
			return swap(next_state, position, position + 3)

	def Node_Expand(self, node):
		# expand node
		expandNode = []
		expandNode.append(self.Node_Create(self.Up(node.state), node, 'Up', node.depth + 1, 0))
		expandNode.append(self.Node_Create(self.Down(node.state), node, 'Down', node.depth + 1, 0))
		expandNode.append(self.Node_Create(self.Right(node.state), node, 'Right', node.depth + 1, 0))
		expandNode.append(self.Node_Create(self.Left(node.state), node, 'Left', node.depth + 1, 0))

		return [i for i in expandNode if i.state != None]

	def bfs(self, initial_state, goal_state):
		# bfs implement
		start_time = time.time()
		nodes = []
		explored = []
		nodes.append(self.Node_Create(initial_state, None, None, 0, 0))
		frontiers = [frontier.state for frontier in nodes]
		self.nodes_expanded = 0
		self.fringe_size = 1
		self.max_fringe_size = 1
		self.max_pops = 0
		self.max_search_depth = 0
		while True:
			if len(nodes)==0: return None
			node = nodes.pop(0)
			explored.append(node.state)
			frontiers.remove(node.state)
			self.max_pops += 1
			if node.state == goal_state:
				tmp_node = node
				self.path_to_goal = []
				self.fringe_size = len(nodes)
				self.search_depth = tmp_node.depth
				while True:
					self.path_to_goal.insert(0, tmp_node.move)
					if tmp_node.depth == 1:
						break
					else: tmp_node = tmp_node.parent
				#self.max_fringe_size = self.fringe_size + self.max_pops
				self.running_time = time.time() - start_time
				self.max_ram_usage = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss / 1000
				return self.path_to_goal
			expanded_nodes = [i for i in self.Node_Expand(node) if i.state not in explored and i.state not in frontiers]
			frontiers.extend([frontier.state for frontier in expanded_nodes])
			nodes.extend(expanded_nodes)
			self.nodes_expanded += 1
			if self.max_fringe_size <= len(nodes):
				self.max_fringe_size = len(nodes)
			if self.max_search_depth < node.depth: 
				self.max_search_depth = node.depth + 1



	def dfs(self, initial_state, goal_state):#, max_depth = float('inf')):
		# dfs implement
		start_time = time.time()
		nodes = []
		explored = []
		nodes.append(self.Node_Create(initial_state, None, None, 0, 0))
		frontiers = [frontier.state for frontier in nodes]
		
		self.nodes_expanded = 0
		self.fringe_size = 1
		self.max_fringe_size = 1
		self.max_pops = 0
		self.max_search_depth = 0
		while True:
			if len(nodes)==0: return None
			node = nodes.pop(0)
			explored.append(node.state)
			frontiers.remove(node.state) 
			self.max_pops += 1

			if node.state == goal_state:
				tmp_node = node
				self.path_to_goal = []
				self.fringe_size = len(nodes)
				self.search_depth = tmp_node.depth
				while True:
					self.path_to_goal.insert(0, tmp_node.move)
					if tmp_node.depth <= 1: break
					tmp_node = tmp_node.parent
				#self.max_fringe_size = self.fringe_size + self.max_pops
				self.running_time = time.time() - start_time
				self.max_ram_usage = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss / 1000
				return self.path_to_goal
			#if node.depth < max_depth:
			expanded_nodes = [i for i in self.Node_Expand(node) if (i.state not in explored and i.state not in frontiers)]
			frontiers.extend([frontier.state for frontier in expanded_nodes])
			
			if len(expanded_nodes) > 0:
				self.nodes_expanded += 1
			expanded_nodes.extend(nodes)
			nodes = expanded_nodes
			if self.max_search_depth < node.depth:
				self.max_search_depth = node.depth
			if self.max_fringe_size <= len(nodes):
				self.max_fringe_size = len(nodes)


	def ast(self, initial_state, goal_state):
		# A-star heuristic search 
		start_time = time.time()
		nodes = []
		explored = []
		nodes.append(self.Node_Create(initial_state, None, None, 0, 0))
		frontiers = [frontier.state for frontier in nodes]
		self.nodes_expanded = 0
		self.fringe_size = 1
		self.max_fringe_size = 1
		self.max_pops = 0
		self.max_search_depth = 0
		self.max_fringe_size = 1
		while True:
			if len(nodes) == 0: return None
			nodes.sort(cmp = compare)
			node = nodes.pop(0)
			explored.append(node.state)
			frontiers.remove(node.state)
			self.max_pops += 1
			if self.max_search_depth <= node.depth:
				self.max_search_depth = node.depth
			if node.state == goal_state:
				tmp_node = node
				self.path_to_goal = []
				self.fringe_size = len(nodes)
				self.search_depth = node.depth
				while True:
					self.path_to_goal.insert(0, tmp_node.move)
					if tmp_node.depth <= 1: break
					tmp_node = tmp_node.parent
				#self.max_fringe_size = self.fringe_size + self.max_pops
				self.running_time = time.time() - start_time
				self.max_ram_usage = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/1000
				return self.path_to_goal

			expanded_nodes = [i for i in self.Node_Expand(node) if i.state not in explored and i.state not in frontiers]
			frontiers.extend([frontier.state for frontier in expanded_nodes])
			self.nodes_expanded += 1
			nodes.extend(expanded_nodes)
			if self.max_fringe_size <= len(nodes):
				self.max_fringe_size = len(nodes)

	def dfs_heuristic(self, node, goal_state, f_limit, depth, nodes_expanded, nodes, explored):
		# dfs with heuristic limit
		depth += 1
		if self.max_search_depth <= depth:
			self.max_search_depth = depth
		self.max_fringe_size = 1
		nodes = nodes
		nodes.append(node)
		explored = explored
		explored.append(node)
		
		if node.cost > f_limit:
			return node, node.cost
		if node.state == goal_state:
			#print(depth, node.depth)
			return node, node.cost
		expanded_nodes = self.Node_Expand(node)
		nodes_expanded += 1
		nodes.extend(expanded_nodes)
		if self.max_fringe_size <= len(nodes):
			self.max_fringe_size = len(nodes)
		minimum = float('inf')
		for i in expanded_nodes:
			i.cost = node.cost + h(i.state, goal_state)
			ret, newlimit = self.dfs_heuristic(i, goal_state, f_limit, depth, nodes_expanded, nodes, explored)
			if ret.state == goal_state:
				self.nodes_expanded = nodes_expanded
				self.fringe_size = len(nodes) - len(explored)
				return ret, ret.cost
			if newlimit < minimum:
				minimum = newlimit
		return node, minimum

	def ida(self, initial_state, goal_state):
		# IDA star implement
		start_time = time.time()
		root_node = self.Node_Create(initial_state, None, None, 0, h(initial_state, goal_state))
		limit = root_node.cost
		nodes_expanded = 0
		notes = []
		self.max_search_depth = 1
		#self.fringe_size = 0
		while True:
			node, new_limit = self.dfs_heuristic(root_node, goal_state, limit, root_node.depth, nodes_expanded, notes, notes)
			limit = new_limit + 1
			if node.state == goal_state:
				tmp_node = node
				self.path_to_goal = []
				#self.fringe_size = len(nodes)
				self.search_depth = tmp_node.depth
				while True:
					self.path_to_goal.insert(0, tmp_node.move)
					if tmp_node.depth <= 1: break
					tmp_node = tmp_node.parent
				self.running_time = time.time() - start_time
				self.max_ram_usage = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/1000
				return self.path_to_goal
		return None







if __name__=="__main__":
	# parse the paramters
	ap = argparse.ArgumentParser()
	ap.add_argument("method", type = str, help = "Search method.")
	ap.add_argument("initial_state", type = str, help = "Initial state.")
	args = ap.parse_args()

	# translate initial state
	state_string = args.initial_state
	initial_state = [int(i) for i in state_string.split(",")]
	goal_state = range(9)

	# method interpreting
	method_string = args.method
	eight_puzzle = EightPuzzles()
	if method_string == "bfs":
		try:
			eight_puzzle.bfs(initial_state, goal_state)
		except Exception:
			pass
	elif method_string == "dfs":
		try:
			eight_puzzle.dfs(initial_state, goal_state)
		except Exception:
			pass
	elif method_string == "ast":
		try:
			eight_puzzle.ast(initial_state, goal_state)
		except Exception:
			pass
	elif method_string == "ida":
		try:
			eight_puzzle.ida(initial_state, goal_state)
		except Exception:
			pass
	else:
		print("Method not found.")



	results = {'path_to_goal': eight_puzzle.path_to_goal,
			   'cost_of_path': len(eight_puzzle.path_to_goal),
			   'nodes_expanded': eight_puzzle.nodes_expanded,
			   'fringe_size': eight_puzzle.fringe_size,
			   'max_fringe_size': eight_puzzle.max_fringe_size,
			   'search_depth': eight_puzzle.search_depth,
			   'max_search_depth': eight_puzzle.max_search_depth,
			   'running_time': round(eight_puzzle.running_time,8),
			   'max_ram_usage': eight_puzzle.max_ram_usage}

	print(results)


	# write to txt file
	f = open('output.txt', "w")
	for item in results.items():
		f.write(str(item[0]) + ": " + str(item[1]) + '\n')
	f.close()



