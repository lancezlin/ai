from autodocparameters import recordparametertype
import autodocparameters
#! /usr/bin/python3

import pymysql
import pymysql.cursors
import yaml

def load_config(filename):
    """"""
    with open(filename, 'r') as conf:
        cfg = yaml.load(conf)
    return cfg

def load_mysql_config(filename):
    """"""
    conf = load_config(filename)
    if not 'mysql' in conf:
        return None
    return conf['mysql']

def get_connection(host, user, password, database, port=3306):
    """"""
    conn = pymysql.connect(host = host,
            user = user,
            password = password,
            database = database,
            port = port,
            cursorclass=pymysql.cursors.DictCursor)
    return conn

@recordparametertype
def create_table(host, user, password, database, query, tablename, port=3306, logger=None):
    """"""

    try:
        conn = get_connection(host, user, password, database, port)
        cur = conn.cursor()
    except Exception as e:
        if logger:
            logger.info("Cannot connect to database with exception - {}.".format(e))
        return
    # drop table
    cur.execute("DROP TABLE IF EXISTS {}".format(tablename))
    # create a new one
    try:
        new_query = query.format(tablename)
        cursor.execute(new_query)
    except Exception as e:
        if logger:
            logger.info("Cannot create table {}".format(tablename))
    finally:
        # close connection

if __name__=='__main__':
    
    pass
