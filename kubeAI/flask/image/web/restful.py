from flask import Flask
from flask import request, jsonify, Response
import json
import pymysql
from flask_cors import CORS, cross_origin
import os
import logging
import pymysql.cursors

app = Flask(__name__)

# ENV variables
MYSQL_USER = 'admin'
MYSQL_PASSWORD = 'test'
MYSQL_DATABASE = 'test'
MYSQL_HOST = 'mysql'
MYSQL_PORT = 3306

# log variables
FLASK_LOG_PATH = os.environ.get('FLASK_LOG_PATH')
if FLASK_LOG_PATH:
    FLASK_LOG = os.path.join(FLASK_LOG_PATH, "flask.log")
else:
    FLASK_LOG = "flask.log"

# add logger to the app
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
handler = logging.handlers.RotatingFileHandler(FLASK_LOG,
        maxBytes=10000,
        backupCount=3)
handler.setFormatter(logging.Formatter(
    '%(asctime)s %(levelname)s: %(message)s '
    '[in %(pathname)s:%(lineno)d]'
))
handler.setLevel(logging.INFO)
logger.addHandler(handler)

# get mysql connector
def mysql_connector():
    """"""
    conn = pymysql.connect(user=MYSQL_USER,
            host=MYSQL_HOST,
            port=MYSQL_PORT,
            password=MYSQL_PASSWORD,
            database=MYSQL_DATABASE,
            cursorclass=pymysql.cursors.DictCursor
            )
    return conn

tables = {}
tables['department'] = (
    "CREATE TABLE `departments` ("
    "  `dept_no` char(4) NOT NULL,"
    "  `dept_name` varchar(40) NOT NULL,"
    "  PRIMARY KEY (`dept_no`), UNIQUE KEY `dept_name` (`dept_name`)"
    ") ENGINE=InnoDB")

@app.route("/")
def index():
    return "Hello world"

@app.route("/api/1.0/monks/table/create", methods=['POST'])
def create_table():
    """"""
    params = request.args
    try:
        connection = mysql_connector()
        cur = connection.cursor()
    except Exception as e:
        logger.info("Can not make the connection to mysql - {}".format(e))
        return "Failed DB connection!"
    # creating table based on the name passed in
    table_name = params.get('name')
    if table_name is None:
        logger.info("Missing table name to create.")
        connection.close()
        return "Missing table name"
    try:
        query = tables['department']
        logger.info("creating table - {}".format(table_name))
        cur.execute(query)
        connection.commit()
        status = 200
        logger.info("{} table created".format(table_name))
    except Exception as e:
        logger.info("Failed to create table - {}".format(table_name))
        status = 500
    finally:
        cur.close()
        connection.close()
    return jsonify({"status": status})

@app.route("/api/1.0/monks/case", methods=['GET'])
def get_case():
    """"""
    params = request.args
    method = request.method
    connection = mysql_connector()
    cur = connection.cursor()
    # if the method is get, return result
    if method == "GET":
        try:
            query = "SELECT * FROM sample;"
            cur.execute(query)
            data = cur.fetchall()
        except Exception as e:
            logger.info("Could not execute the query.")
        finally:
            cur.close()
            connection.close()
    return jsonify(result=data)

@app.route("/api/1.0/databases/show", methods=['GET'])
def show_databases():
    """"""
    logger.info("Showing databases.")
    connection = mysql_connector()
    cur = connection.cursor()
    try:
        query = "SELECT * FROM sample;"
        cur.execute(query)
        data = cur.fetchall()
    except Exception as e:
        logger.info("Failed to show databases.")
    finally:
        cur.close()
        connection.close()
    return jsonify(result=data)

if __name__=='__main__':
    # run the apps
    app.run(debug=True, host='0.0.0.0')
