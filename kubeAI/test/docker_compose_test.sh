#! /bin/bash

# go to the working directory
cd /home/lancel/Projects/ai/kubeAI/

# bring down docker compose
docker-compose down

# remove images
docker rmi kubeai_flask
docker rmi kubeai_mysql

# compose docker up again
docker-compose up --build
