#! /usr/bin/python

import argparse
import os
import sys
import signal
import util
from crontab import CronTab
from util import load_yaml
from util import get_file_paths
import configparser

class LoadJob(object):
    """"""

    def parse_args(self, arg):
        parser = argparse.ArgumentParser()
        parser.add_argument('--config', action='store', default='/image/config/jobs/', help='config path to the jobs')
        self.args = parser.parse_args(arg)

    def __init__(self, arg):
        """"""
        self.parse_args(arg)
        self.job_conf_path = self.args.config
        self.cron = CronTab(user='root')

    def write_jobs_to_tab(self):
        """"""
        print ("the job config path {}".format(self.job_conf_path))
        job_config_files = get_file_paths(self.job_conf_path)
        job_names = []
        for job_file in job_config_files:
            try:
                job_conf = load_yaml(job_file)
                job_name = job_conf['report_name']
                schedule = job_conf['schedule']
                script_path = job_conf.get('script_path', '/')
                if job_name in job_names:
                    print ("Duplicated report name {}, skiped!".format(job_name))
                    continue
                job_names.append(job_name)
            except Exception as e:
                print ("Cannot load job config file {} - {}".format(job_file, e))
                continue
            cmd = "/usr/bin/python3 {0} --config {1} {2}".format(script_path, job_file, ">> /var/log/cron.log 2>&1")
            try:
                job = self.cron.new(command=cmd)
                job.comment = job_name
                job.setall(schedule)
                self.cron.write()
            except Exception as e:
                continue
        return True

    def run(self):
        self.write_jobs_to_tab()

def main(args=None):
    # establish the client
    # signal.signal(signal.SIGINT, handle_signal)
    if args is None:
        args = sys.argv[1:]
    client = LoadJob(args)
    client.run()

if __name__=='__main__':
    sys.exit(main(args=sys.argv[1:]))
