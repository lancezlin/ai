#! /usr/bin/python

import util
from util import load_ini_conf
from util import load_yaml
import os
import sys
import subprocess
import argparse
import signal
import logging
import logging.handlers

# log variables
MAIL_LOG_PATH = os.environ.get('LOG_PATH')
if MAIL_LOG_PATH:
    MAIL_LOG = os.path.join(MAIL_LOG_PATH, "mail.log")
else:
    MAIL_LOG = "mail.log"

# add logger to the app
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
handler = logging.handlers.RotatingFileHandler(MAIL_LOG,
        maxBytes=10000,
        backupCount=3)
handler.setFormatter(logging.Formatter(
    '%(asctime)s %(levelname)s: %(message)s '
    '[in %(pathname)s:%(lineno)d]'
))
handler.setLevel(logging.INFO)
logger.addHandler(handler)

class MailAPICaller(object):
    """"""
    def __init__(self, arg):
        logger.info("Initiating the mail api ...")
        self._parse_args(arg)
        self.config = self._load_job_config(self.args.config)
        self.cmd = self.form_cmd()
        logger.info("Command to call - {} ...".format(self.cmd))
        pass

    def _load_job_config(self, filename):
        """"""
        return load_yaml(filename)

    def _parse_args(self, arg):
        parser = argparse.ArgumentParser()
        parser.add_argument('--config', action='store', default='/image/config/jobs/', help='config path to the jobs')
        self.args = parser.parse_args(arg)

    def form_api_url(self):
        url = "{}/{}".format(self.config['api_base_url'], self.config['api_end_point'])
        return url

    def form_cmd(self):
        """"""
        url = self.form_api_url()
        method = self.config.get('method')
        params = self.config.get('params', {})
        if method:
            base_cmd = "curl -X {0} {1}".format(method, url)
        else:
            base_cmd = "curl -X GET {}".format(url)
        if params:
            for p, v in params.items():
                base_cmd += "?" + str(p) + "=" + str(v)
        return base_cmd


    def call(self):
        logger.info("Calling the mail api ...")
        subprocess.Popen(self.cmd, shell=True)

def main(args=None):
    # establish the client
    # signal.signal(signal.SIGINT, handle_signal)
    if args is None:
        args = sys.argv[1:]
    client = MailAPICaller(args)
    client.call()

if __name__=='__main__':
    sys.exit(main(args=sys.argv[1:]))

