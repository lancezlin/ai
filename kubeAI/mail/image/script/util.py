#! /usr/bin/python
import os
import pandas as pd
import numpy as np
from staticconf.loader import yaml_loader
import yaml
import configparser

# helper functions
def foo():
    print ("Testing foo function!!")

# yaml loader
def load_yaml(filename):
    """"""
    try:
        loaded = yaml_loader(filename)
    except yaml.scanner.ScannerError as e:
        raise EAException('Could not parse file %s: %s' % (filename, e))
    return loaded

# get the file/conf paths as list
def get_file_paths(conf):
    """"""
    if isinstance(conf, dict):
        folder = conf['job_folder']
    else:
        folder = conf
    job_confs = []
    for root, folders, files in os.walk(folder):
        print ("Root {} + folder {} + file {}".format(root, folders, files))
        for filename in files:
            if isyaml(filename):
                job_confs.append(os.path.join(root, filename))
            else:
                continue
    return job_confs

# check if yaml file
def isyaml(filename):
    """"""
    return filename.endswith('.yaml') or filename.endswith('.yml')

# load ini conf
def load_ini_conf(filename):
    """"""
    try:
        conf = configparser.ConfigParser()
        conf.read(filename)
    except Exception as e:
        print ("Cannot load config {} - {}".format(filename, e))
        return {}
    return conf
