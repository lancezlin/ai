#! /bin/bash

# Env variable
if [ -z "${LC_CONFIG_PATH}" ]; then
    LC_CONFIG_PATH="/image/config/lc_mail.yaml"
fi

# Load the configurations based upon the env vars
if [ -n "${LC_ENV}" ]
then
    if [ "${LC_ENV}" -eq "prod" ]
    then
	echo "Loading configurations for production env."
	sed -i -e "s|||g" ${LC_CONFIG_PATH}
    elif [ "${LC_ENV}" -eq "dev" ]
    then
	echo "Loading configurations for dev env."
	sed -i -e "s|||g" ${LC_CONFIG_PATH}
    elif [ "${LC_ENV}" -eq "test" ]
    then
	echo "Loading configurations for test env."
	sed -i -e "s|||g" ${LC_CONFIG_PATH}
    else
	echo "Loading configurations for other env."
	# sed -i -e "s|||g" ${LC_CONFIG_PATH}
    fi
fi

# export the util python path
export PYTHONPATH=/python3:/utils

# execute the supervisord to launch the project
exec supervisord -c /etc/supervisord.conf -n
