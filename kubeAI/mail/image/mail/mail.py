from flask import Flask
from flask import request, jsonify, Response
import yaml
import json
import cx_Oracle
from flask_cors import CORS, cross_origin
import os
import logging
from staticconf.loader import yaml_loader

from os.path import basename
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE
from email.utils import formatdate
from smtplib import SMTP
from smtplib import SMTP_SSL
from smtplib import SMTPAuthenticationError
from smtplib import SMTPException
from smtplib import SMTPResponseException
from socket import error
from util import load_yaml

app = Flask(__name__)

# log variables
FLASK_LOG_PATH = os.environ.get('LOG_PATH')
if FLASK_LOG_PATH:
    FLASK_LOG = os.path.join(FLASK_LOG_PATH, "mail.log")
else:
    FLASK_LOG = "mail.log"

# add logger to the app
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
handler = logging.handlers.RotatingFileHandler(FLASK_LOG,
        maxBytes=10000,
        backupCount=3)
handler.setFormatter(logging.Formatter(
    '%(asctime)s %(levelname)s: %(message)s '
    '[in %(pathname)s:%(lineno)d]'
))
handler.setLevel(logging.INFO)
logger.addHandler(handler)

# loading the configurations
logger.info("Loading the configs ... ")
config_file_path = '/image/config/lc_mail.yaml'
configs = load_yaml(config_file_path)
logger.info("Configuration loaded {0}".format(configs))

# TODO create a function to return the files to send
def get_attach_files():
    """"""
    files = []
    files.append('/image/test/sample.csv')
    files.append('/image/test/sample.txt')
    return files

def create_msg(from_addr, to_addr, subject, body, files):
    """"""
    msg = MIMEMultipart()
    msg['From'] = from_addr
    msg['To'] = COMMASPACE.join(to_addr)
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject
    msg.attach(MIMEText(body))
    # adding the attachments
    for f in files or []:
        with open(f, "rb") as attach:
            part = MIMEApplication(
                attach.read(),
                Name=basename(f))
        # After the file is closed
        part['Content-Disposition'] = 'attachment; filename="%s"' % basename(f)
        msg.attach(part)
    return msg

@app.route("/")
def index():
    return "Hello world"

@app.route("/api/1.0/reports/mail", methods=['GET'])
def mail_report():
    """"""
    logger.info("Start sending report to email...")
    try:
        logger.info("Loading the configurations ...")
        host = configs.get('smtp_host', 'localhost')
        port = configs.get('smtp_port', '465')
        user = configs.get('smtp_username')
        password = configs.get('smtp_password')
        use_ssl = configs.get('smtp_ssl', True)
        to_addr = configs.get('recipient', None)
        from_addr = configs.get('from_addr', user)
    except Exception as e:
        logger.info("Cannot load mail server configs: {}".format(e))
        return jsonify(status=200)
    logger.info("Constructing SMTP client ...")
    tls_retry = False
    try:
        try:
            if use_ssl:
                if port:
                    smtp_client = SMTP_SSL(host, port)
                else:
                    smtp_client = SMTP_SSL(host)
        except:
            tls_retry = True
        if not use_ssl or tls_retry:
            if port:
                smtp_client = SMTP(host, port)
            else:
                smtp_client = SMTP(host)
            smtp.ehlo()
            if smtp.has_extn('STARTTLS'):
                smtp.starttls()
        if user and password:
            smtp_client.login(user, password)
            logger.info("Mail client constructed!")
    except (SMTPException, error) as e:
        logger.info("Cannot connect to mail server - {}".format(e))
        res = jsonify(status = 500,
                mimetype = 'application/json',
                response = 'Failed connecting to SMTP host.')
        return res
    logger.info("Constructing the mail content ...")
    try:
        subject = "Report: testing mail from {}".format(from_addr)
        body = "You got it!!"
        files = get_attach_files()
        message = create_msg(from_addr, to_addr, subject, body, files)
        smtp_client.sendmail(from_addr, to_addr, message.as_string())
        smtp_client.close()
        res = jsonify(status = 200,
                mimetype = 'application/json',
                response = 'Mail send successfully!')
        logger.info("Mail sent to {}".format(to_addr))
    except SMTPException as e:
        logger.info("Failed sending mail to {}".format(to_addr))
        res = jsonify(status = 400,
                mimetype = 'application/json',
                response = 'Failed sending mail.')
    return res


if __name__=='__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)
