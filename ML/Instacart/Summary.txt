Data exploration and preprocessing:
- adding a few features / stats:
	- total_unique_items: number of items by trip
	- total_departments: number of departments by trip
	- total_quantities: total number of items by trip
	- avg_items_per_dpmt: order_details.total_unique_items / order_details.total_departments
	- avg_quant_per_dpmt: order_details.total_quantities / order_details.total_departments
	- avg_quant_per_item: order_details.total_quantities / order_details.total_unique_items
	- total_trips_by_shopper: for each shopper, their total trips by now
	- store_by_shopper: number of stores that each shopper covers
- most of the features are not correlated
- split training data into train/test (7:3) subset to evaluate the model.

Model:
XGBoost + Grid search cross validation:
- XGBoost is a widely used ensemble ML technique for prediction. It's easy to implement and efficient.
- Grid search CV can help select the best parameter combinations and defeat the overfitting issue (l2 regularization).

Performance:

Mean absolute error of the best model in training subset: 768.857024508.
Mean squared error of the best model in training subset: 1128736.82883.


Mean absolute error of the best model in testing subset: 792.715344136.
Mean squared error of the best model in testing subset: 1233361.04335.

Alternatives/concerns:
Maybe can try Neural Network models.
