#! /usr/bin/env python

class Burrito():
	def __init__(self, meat, to_go, rice, beans, extra_meat=False, guacamole=False, cheese=False, pico=False, corn=False):
		self.set_meat(meat)
		self.set_to_go(to_go)
		self.set_rice(rice)
		self.set_beans(beans)
		self.set_extra_meat(extra_meat)
		self.set_guacamole(guacamole)
		self.set_cheese(cheese)
		self.set_pico(pico)
		self.set_corn(corn)
		
	def set_meat(self, meat):
		self.meat = Meat(meat)
	def set_to_go(self, to_go):
		if to_go in [True, False]:
			self.to_go=to_go
		else:
			self.to_go=False
	def set_rice(self, rice):
		self.rice = Rice(rice)
	def set_beans(self, beans):
		self.beans = Beans(beans)
	def set_extra_meat(self, extra_meat=False):
		if extra_meat in [True, False]:
			self.extra_meat=extra_meat
		else:
			self.extr_meat=False
	def set_guacamole(self, guacamole=False):
		if guacamole in [True, False]:
			self.guacamole=guacamole
		else:
			self.guacamole=False
	def set_cheese(self, cheese=False):
		if cheese in [True, False]:
			self.cheese=cheese
		else:
			self.cheese=False
	def set_pico(self, pico=False):
		if pico in [True, False]:
			self.pico=pico
		else:
			self.pico=False
	def set_corn(self, corn=False):
		if corn in [True, False]:
			self.corn=corn
		else:
			self.corn=False

	def get_meat(self):
		return self.meat.get_value()
	def get_to_go(self):
		return self.to_go
	def get_rice(self):
		return self.rice.get_value()
	def get_beans(self):
		return self.beans.get_value()
	def get_extra_meat(self):
		return self.extra_meat
	def get_guacamole(self):
		return self.guacamole
	def get_cheese(self):
		return self.cheese
	def get_pico(self):
		return self.pico
	def get_corn(self):
		return self.corn



	def get_cost(self):
		self.cost = 5
		if self.meat.get_value()=="steak":
			self.cost += 1.5
		elif self.meat.get_value() != False:
			self.cost += 1.0
		if self.extra_meat and self.meat.get_value() in ["chicken", "pork", "steak", "tofu"]:
			self.cost += 1.0
		if self.guacamole:
			self.cost += 0.75

		return self.cost


class Meat:
	def __init__(self, value=False):
		self.set_value(value)
			
	def get_value(self):
		return self.value
	
	def set_value(self, value):
		if value in ["chicken", "pork", "steak", "tofu"]:
			self.value = value
		else:
			self.value = False

class Rice:
	def __init__(self, value=False):
		self.set_value(value)
			
	def get_value(self):
		return self.value
	
	def set_value(self, value):
		if value in ["brown", "white"]:
			self.value = value
		else:
			self.value = False
			
class Beans:
	def __init__(self, value=False):
		self.set_value(value)
			
	def get_value(self):
		return self.value
	
	def set_value(self, value):
		if value in ["black", "pinto"]:
			self.value = value
		else:
			self.value = False



def total_cost(burritos):
	t_cost = 0
	for b in burritos:
		t_cost += b.get_cost()
	return t_cost

def phoneBook(names, numbers):
	phone_book = {}
	for i in range(len(names)):
		phone_book[names[i]]=numbers[i]
	return phone_book

def french2eng(sentence):
	words = sentence.lower().split(' ')
	new_words = []
	for w in words:
		if w in frenchDict:
			new_words.append(frenchDict[w])
		else:
			new_words.append(w)
	return ' '.join(new_words)

def courseInfo(listOfStudents):
	students, sumAge, course_info = [], 0, {}
	for tup in listOfStudents:
		students.append(tup[0])
		sumAge += tup[1]
	course_info['students'] = students
	course_info['avgAge'] = 1.0*sumAge/len(listOfStudents)
	return course_info

# Q1
#Python, like most languages, actually uses numbers in the 
#background to represent individual characters in a string. 
#For example, "a" is assigned the numeric value of 97. 
#We call this the ordinal value. 
#
#Write a function called "ordinal_value" that determines if 
#a given string of length one has an ordinal value of 
#greater than 100 and less than 200. Return the boolean 
#result. 
#
#Hint: you can find the ordinal value of a character using 
#a built in python function like so: ord("a").
#
#Note: you may not declare any strings in your solution.
def ordinal_value(input_str):
	if ord(input_str)>100 and ord(input_str)<200:
		return True
	return False

# Q2
#Write function called third_index that accepts a string
#as an argument and returns just the third character of
#the string. If the user inputs a string with fewer than
#3 characters, return "too short". 

def third_index(input_str):
	try:
		return input_str[2]
	except IndexError:
		return("too short")

# Q3
#Write a function called "last_n" that accepts two arguments:
#a string searchString and an integer n. The function should
#return the last n characters frm searchString. If
#searchString is shorter than n characters, then it should
#return the entire value of searchString.
def last_n(searchString, n):
	if len(searchString) < n:
		return searchString
	return searchString[len(searchString)-n:]

# Q4
#Write a function called "in_parentheses" that accepts a 
#single argument. You can expect that the function will 
#given a sentence which contains some words in parentheses. 
#Your function should return the contents of the 
#parentheses.

#For example:
#in_parentheses("This is a sentence (words!)") -> "words!"

#You may assume there is only one set of parentheses in the
#string. If both parentheses do not appear, return an empty
#string.
def in_parentheses(input_str):
	l, r, i = -1, -1, 0
	while i < len(input_str):
		if input_str[i] == "(":
			l = i
		if input_str[i] == ")":
			r = i
		i += 1
	if l < r and l >= 0 and r >= 0: return input_str[l+1: r]
	return ""

# Q5
#Write a function called 'str_type' which accepts one string 
#argument and determines what type of string it is.  
#
#If the string is a single character, return "character". 
#If the string contains a single word, return "word". 
#If the String contains a whole sentence, return "sentence".
#
#You may assume a sentence is anything with more than one word
#in it, and that multiple words will always be separated by
#spaces. You may also assume the string has at least one
#character (no blank strings).
def str_type(input_str):
	if len(input_str)==1: return "character"
	elif " " in input_str:
		return "sentence"
	else:
		return "word"

# Q6
#Recall that input from a user is always in the form of a string. 
#Write a function  called "input_type" that gets user input and 
#determines what kind of string the user entered.
#
#Note that because you're getting user input, you will not be able
#to run this code: the only way you can test it is to submit it.
#When you submit your code, we'll test it with some simulated
#input.
#
#  - Your function should return "integer" if the string only
#    contains characters 0-9.
#  - Your function should return "float" if the string only
#    contains the numbers 0-9 and at most one period.
#  - You should return "boolean" if the user enters "True" or
#    "False". 
#  - Otherwise, you should return "string".
#
#Remember, start the input_type() function by getting the user's
#input using the input() function. The call to input() should be
#*inside the* input_type() function.
def input_type():
	user_input = str(input())
	if user_input.isdigit():
		return "integer"
	elif user_input.replace(".", "").isdigit() and len(user_input.replace(".", ""))==(len(user_input)-1):
		return "float"
	elif user_input == "True" or user_input=="False":
		return "boolean"
	else:
		return "string"

# Q7
#This one is a challenge. There's a lot going on: splitting
#up strings, removing unnecessary characters, converting to
#integers, and running a big conditional. You can do it!
#
#In web development, it is common to represent a color like 
#this:
#
#  rgb(red_val, green_val, blue_val)
#
#where red_val, green_val and blue_val would be substituted 
#with values from 0-255 telling the computer how much to 
#light up that portion of the pixel. For example:
#
# - rgb(255, 0, 0) would make a color red. 
# - rgb(255, 255, 0) would make yellow, because it is equal 
#   parts red and green. 
# - rgb(0, 0, 0) would make black, the absence of all color.
# - rgb(255, 255, 255) would make white, the presence of all
#   colors equally.
#
#Don't let the function-like syntax here confuse you: here,
#these are just strings. The string "rgb(0, 255, 0)"
#represents the color green.
#
#Write a function called "find_color" that accepts a single 
#argument expected to be a string as just described. Your
#function should return a simplified version of the color
#that is represented according to the following rules:
#
# If there is more red than any other color, return "red".
# If there is more green than any other color, return "green".
# If there is more blue than any other color, return "blue".
# If there are equal parts red/green, return "yellow".
# If there are equal parts red/blue, return "purple".
# If there are equal parts green/blue, return "teal".
# If there are equal parts red/green/blue, return "gray".
def find_color(pixel):
	values = pixel.split("(")[1].split(")")[0].split(",")
	r, g, b = int(values[0]), int(values[1]), int(values[2])
	if r == g == b > 0: return "gray"
	if r == g == b == 0: return "black"
	if r == g == b == 255: return "white"
	if r > g and r > b: return "red"
	if g > r and g > b: return "green"
	if b > g and b > r: return "blue"
	if b == g != r: return "teal"
	if b != g == r: return "purple"
	if b == r != g: return "yellow"

#Q8
#Write a function called hideAndSeek. It should take no
#parameters and return no output. Instead, when called, this
#function should print the numbers 1 through 10, and then
#print the text "Ready or not, here I come!".
def hideAndSeek():
	for i in range(1,11):
		print (i)
	print ("Ready or not, here I come!")

# Q9
#In the previous coding problem, you created a function
#called hideAndSeek that printed the numbers from 1 to 10.
#Now, however, we want to extend that. What if we want to
#count to 20? 30?
#
#Modify your previous function so that it takes as input one
#parameter: count. Then, instead of printing the numbers from
#1 to 10, it should print the numbers from 1 to the value of
#count. Then, end with "Ready or not, here I come!"
def hideAndSeek(count):
	for i in range(1, count+1):
		print(i)
	print("Ready or not, here I come!")

# Q10
#A year is considered a leap year if it abides by the
#following rules:
#
#  - Every 4th year IS a leap year, EXCEPT...
#  - Every 100th year is NOT a leap year, EXCEPT...
#  - Every 400th year IS a leap year.
#
#This starts at year 0. For example:
#
#  - 1993 is not a leap year because it is not a multiple of 4.
#  - 1996 is a leap year because it is a multiple of 4.
#  - 1900 is not a leap year because it is a multiple of 100,
#    even though it is a multiple of 4.
#  - 2000 is a leap year because it is a multiple of 400,
#    even though it is a multiple of 100.
#
#Write a function called isLeapYear. isLeapYear should take
#one parameter: year, an integer. It should return True if
#that year is a leap year, False if it is not.
def isLeapYear(year):
	if year % 400 == 0: return True
	elif year % 100 == 0: return False
	elif year % 4 == 0:return True
	else: return False

# Q11
#Write a function called luckySevens() that takes in one
#parameter, a string variable named aStr. Your function
#should return True if there are exactly three '7's in aStr.
#If there are less than three  or more than three '7's, the
#function should return False.
#
#For example:
#  - luckySevens("happy777bday") should return True.
#  - luckySevens("h7app7ybd7ay") should also return True.
#  - luckySevens("happy77bday") should return False.
#  - luckySevens("h777appy777bday") should also return False.

def luckySevens(aStr):
	num_sevens = 0
	for i in range(len(aStr)):
		if aStr[i] == "7":
			num_sevens += 1
	if num_sevens == 3:
		return True
	return False

# Q12
#In chemistry, the ideal gas law states:
#
# pressure * volume = # of moles * gas constant * temperature
#
#This is usually abbreviated to:
#
# PV = nRT
#
#We can solve this for any of these five variables, but let's
#solve it for Pressure. In terms of Pressure, the ideal gas
#law states:
#
# P = (nRT) / V
#
#Write a function called findPressure that takes as input
#three variables: number of moles, temperature, and volume.
#You can call these variables in the function whatever you
#want, but they must be specified in that order: moles, then
#temperature, then volume. You should assume all three are
#floats. Then, return as output your calculation for
#pressure. For the gas constant, you should use the value 
#0.082057.
def findPressure(m, t, v, R=0.082057):
	try:
		return m*R*t/v
	except ZeroDivisionError:
		return "Volume must be greater than 0."
	


# Q13
#This program is supposed to print the location of the 'o'
#in each word in the list. However, line 14 generates an
#error when 'o' is not in the word. Add try/except blocks
#to print "Not found" when the word does not have an 'o'.
#However, when the current word does not have an 'o', the
#program should keep going on to the next word.
#
#You may not use any conditionals.

words = ["dog", "ago", "cat", "fog"]

for word in words:
	try:
		print(word.index("o"))
	except:
		print("Not found")


# Q14
#Write a program that divides mysteryValue by mysteryValue
#and print the result. If that operation results in an
#error, divide it by (mysteryValue - 5) and print the
#result. If that still fails, multiply mysteryValue by 5
#and print the result. You may assume one of those three
#things will work.
#
#You may not use any conditionals.
#
#Hint: You're going to want to test one try/except structure
#inside another! Think carefully about whether the second
#one should go inside the try block or the except block.

import sys
try:
	mysteryValue = int(sys.argv[1])
except:
    mysteryValue = sys.argv[1]
print("~ testing with mysteryValue = {} ~".format(mysteryValue))

try:
	print(mysteryValue/mysteryValue)
except:
	try:
		print(mysteryValue/(mysteryValue-5))
	except:
		print(mysteryValue*5)

# Q15
#Write a program that iterates through the items in the
#given list. For each item, you should attempt to iterate
#through the item and print each character seperately. 
#
#If this fails, print "Not iterable".
#
#Hint: Although we'll cover lists more in Unit 4, all
#you need to know right now is that this syntax will run
#a loop over a list, a string, or any other iterable
#type of information:
#
#  for item in givenItems:
#
#To iterate over the items in 'item', you can do the
#same thing. Start out by building the nested for-each
#loops that you'll need, then figure out where to put
#the try-except structure.
#
#This one's tricky, but you can do it!

givenItems = ["one", "two", 3, 4, "five", ["six", "seven", "eight"]]

for item in givenItems:
	try:
		for i in item:
			try:
				for j in i:
					print (j)
			except:
				print(i)
	except:
		print("Not iterable")

# Q16
#We've come a long way in this unit! You've learned about
#conditionals, loops, functions, and error handling. To end
#the unit, let's do a couple problems that tie all these
#concepts together.
#
#Write a function called wordCount. wordCount takes as input
#a string called myString, and returns as output the number
#of words in the string. For the purposes of this problem,
#you can assume that every space indicates a new word; so,
#the number of words should be one more than the number of
#spaces.
#
#Note, though, that it could be the case that a non-string
#is accidentally passed in as the argument for myString. If
#that happens, an error will arise. If such an error arises,
#you should instead return, "Not a string". Otherwise,
#return an integer representing the number of words in the
#string.
#Revise your wordCount method so that if it encounters
#multiple consecutive spaces, it does *not* count an
#additional word. For example, these three strings should
#all be counted as having two words:
#
# "Hi David"
# "Hi   David"
# "Hi                 David"
#
#Other than ignoring consecutive spaces, the directions are
#the same: write a function called wordCount that returns an
#integer representing the number of words in the string, or
#return "Not a string" if the input isn't a string. You may
#assume that if the input is a string, it starts with a
#letter word instead of a space.
def wordCount(myString):
	numSpace = 0
	try:
		last_space_index = 0
		dup = 0
		for i in range(len(myString)):
			if myString[i] == " ":
				if i == last_space_index + 1:
					dup += 1
				last_space_index = i
				numSpace += 1
		return numSpace - dup + 1
	except:
		return "Not a string"


# Q17
#This will be the largest, most authentic program you've
#written so far. It will require everything you've learned
#and should take some time to test and debug. Be patient,
#you can do it!
#
#Write a function called averageWordLength that takes as
#input a string called myString, and returns as output the
#average length of the words in the string.
#
#In writing this function, note the following:
#
# - You should account for consecutive spaces. A string like
#   "Hi   Lucy" is two words with an average length of 3.0
#   characters.
# - You should not assume the string starts with a letter.
#   A string like "  David" has one words with an average
#   length of 5.0 characters.
# - You should not count punctuation marks toward the
#   length of a word. A string like "Hi, Lucy" has two
#   words with an average length of 3.0 characters: the ,
#   after "Hi" does not count as a character in the word.
#   The only punctuation marks you need to handle are
#   these: . , ! ?
# - You may assume the only characters in the string are
#   letters, the punctuation marks listed above, and spaces.
# - If myString is not a string, you should instead return
#   the string, "Not a string".
# - If there are no words in myString, you should instead
#   return the string, "No words". This could happen for
#   strings like "" (an empty string) and ".,!?" (a string
#   of only punctuation marks). You may assume that any
#   of these punctuation marks will always be followed by
#   at least one space.
#
#Here are a few hints that might help you:
#
# - You can peak at the first character in myString with
#   myString[0]. If myString is "Hi, Lucy", then the value
#   of myString[0] is "H".
# - There are lots of ways you can do this. If you're
#   stuck, try taking a step back and thinking about the
#   problem from a fresh perspective.
# - The word count should equal the number of letters that
#   come immediately after a space or the start of the
#   string. The character count should simply equal the
#   number of characters besides spaces and punctuation
#   marks. The average word length should be character
#   count divided by word count.
def averageWordLength(myString):
	try:
		myString_wo_punc = myString.replace(",","").replace(".","").replace("?","").replace("!","")
		print(myString_wo_punc)
		if len(myString_wo_punc)==0: 
			return "No words"
		
		else:
			last_char_index = 0
			num_char = 0
			num_word = 0
			for i in range(len(myString_wo_punc)):
				if myString_wo_punc[i] != " ":
					if i==0 or i - last_char_index > 1:
						num_word += 1
						print(i, ": ", num_word)
					last_char_index = i
					num_char += 1
					print(last_char_index, " ", num_char)
			if num_word == 0: return "No words"
			print(num_char/num_word)
			return num_char/num_word
	except:
		return "Not a string"



if __name__=="__main__":

	#print("Created a Burritos class!!")

	#print(courseInfo([("Jackie", 20), ("Marguerite", 21)]))

	print(ordinal_value("a"))
	print(ordinal_value("o"))

	print(third_index("abc"))
	print(third_index("ab"))

	print(last_n("123456789", 3))
	print(last_n("Bulbasaur", 4))
	print(last_n("1", 5))

	print(in_parentheses("This is a sentence (words!)."))
	print(in_parentheses("No parentheses here!"))
	print(in_parentheses("David tends to speak in a lot of parentheticals (as he is doing right now). It tends to be quite annoying."))

	print(str_type("a"), str_type("a b"), str_type("djjdh"))

	print(input_type())

	print(find_color("rgb(125, 50, 75)"))
	print(find_color("rgb(125, 17, 125)"))
	print(find_color("rgb(217, 217, 217)"))


	testStringTrue = "happy777bday"
	print(testStringTrue, luckySevens(testStringTrue))
	testStringFalse1 = "happy77bday"
	print(testStringFalse1, luckySevens(testStringFalse1))
	testStringFalse2 = "h777app7y77bday"
	print(testStringFalse2, luckySevens(testStringFalse2))


	print("Word Count:", wordCount("Four words are here!"))
	print("Word Count:", wordCount("One."))
	print("Word Count:", wordCount("There are seven words in this sentence."))
	print("Word Count:", wordCount(5))
	print("Word Count:", wordCount(5.1))
	print("Word Count:", wordCount(True))