

''' moving one backwards '''

def Insert(arr, k, x):
	_size = len(arr)
	new_arr = [0]*(_size+1)
	while _size >= k:
		new_arr[_size] = arr[_size-1]
		_size -= 1
	new_arr[k] = x
	i = 0
	while i < k:
		new_arr[i] = arr[i]
		i += 1
	return new_arr


''' deleting a block of array '''
def deleteBlock(arr, lo, hi):
	n = len(arr)
	new_arr = []
	i = 0
	if lo == hi:
		return False
	while i <= n:
		if i < lo:
			new_arr.append(arr[i])
		elif i < n - hi + lo:
			new_arr.append(arr[i + hi - lo])
		else:
			return new_arr
		i += 1

''' deduplicating vector'''


''' finding '''

''' traversing '''


if __name__=="__main__":

	arr = [2,3,4,5,6,6,3,4,9]
	k =3
	x = 10

	print("Insert {0} at position {1} to {2}: ".format(x, k, arr))
	print("result: {0} ".format(Insert(arr, k, x)))
	print("deleting 2 - 4: {0}".format(deleteBlock(arr, 2, 3)))