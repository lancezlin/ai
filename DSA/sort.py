import time

class Sort(object):
	"""docstring for Sort"""
	

	def swap(self, aList, x, y):
		tmp = aList[x]
		aList[x] = aList[y]
		aList[y] = tmp
		return aList


	def bubbleSort(self, data):
		# Bubble sort
		n = len(data)
		for i in range(n-1):
			swapped = False
			for j in range(n-1):
				if data[j] > data[j+1]:
					tmp = data[j]
					data[j] = data[j+1]
					data[j+1] = tmp
					swapped = True
			if not swapped:
				break
		return data

	def insertionSort(self, data):
		# Insertion sort
		n = len(data)
		for i in range(n):
			toInsert = data[i]
			holePos = i
			while i > 0 and data[holePos-1] > toInsert:
				data[holePos] = data[holePos-1]
				holePos -= 1
			data[holePos] = toInsert
		return data

	def selectSort(self, data):
		# Selection sort
		n = len(data)
		for i in range(n-1):
			minIndex = i
			for j in range(i+1, n):
				if data[j] < data[minIndex]:
					minIndex = j
			if minIndex != i:
				tmp = data[i]
				data[i] = data[minIndex]
				data[minIndex] = tmp
		return data


	def mergeSort(self, data):
		# Merge sort
		n = len(data)
		if n==1: return data

		list1, list2= data[0: n/2], data[n/2: n]
		list1, list2 = self.mergeSort(list1), self.mergeSort(list2)

		final_list = self._merge(list1, list2)
		return final_list

	def _merge(self, l1, l2):
		# Merge function for merge sort
		c = []
		while len(l1)>0 and len(l2)>0:
			if l1[0] < l2[0]:
				c.append(l1[0])
				l1.remove(l1[0])
			else:
				c.append(l2[0])
				l2.remove(l2[0])

		while len(l1) > 0:
			c.append(l1[0])
			l1.remove(l1[0])
		while len(l2) > 0:
			c.append(l2[0])
			l2.remove(l2[0])

		return c

	def shellSort(self, data):
		# Shell sort 
		interval = 1
		n = len(data)

		while interval < n/3:
			interval = 3 * interval + 1

		while interval > 0:
			outer = interval
			while outer < n:
				toInsert = data[outer]
				inner = outer
				while inner > interval - 1 and data[inner - interval] > toInsert:
					data[inner] = data[inner - interval]
					inner = inner - interval

				data[inner] = toInsert

				outer += 1
			interval = (interval - 1)/3 

		return data

	def quickSort(self, data, left, right):
		# Quick sort
		if right - left <= 0:
			return
		else:
			pivot = data[right]
			partition, data = self.pivot(left, right, pivot, data)
			self.quickSort(data, left, partition-1)
			self.quickSort(data, partition+1, right)
		return data

	def pivot(self, left, right, pv, Arr):
		# Pivot function support quick sort algo
		left_point = left
		right_point = right - 1
		while True:
			while Arr[left_point] < pv:
				left_point += 1
			while Arr[right_point] > pv and right_point > 0:
				right_point -= 1

			if left_point >= right_point:
				break
			else:
				Arr = self.swap(Arr, left_point, right_point)

		Arr = self.swap(Arr, left_point, right)

		return left_point, Arr





def main():
	"""test"""
	testL = [50, 8, 9, 4, 3, 8, 2, 40, 22, 15, 30, 6]
	sort = Sort()

	bubble_list = sort.bubbleSort(testL)
	insert_list = sort.insertionSort(testL)
	select_list = sort.selectSort(testL)
	merge_list = sort.mergeSort(testL)
	shell_list = sort.shellSort(testL)

	# quick sort
	quick_list = sort.quickSort(testL, 0, len(testL)-1)

	print("bubble sorted: ", bubble_list)
	print("insertion sort: ", insert_list)
	print("select sorted: ", select_list)
	print("merge sorted: ", merge_list)
	print("shell sorted: ", shell_list)
	print("quick sorted: ", quick_list)
if __name__=="__main__":
	main()