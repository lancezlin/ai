#! /usr/bin/python

class BackPack(object):
    """"""
    def __init__(self, arr, m):
        self.arr = arr
        self.m = m

    def capacity(self):
        self.dp = [0] * (self.m+1)
        for i in range(len(self.arr)):
            j = self.m
            while j > 0:
                if j >= self.arr[i]:
                    self.dp[j] = max(self.dp[j], self.dp[j-self.arr[i]]+self.arr[i])
                j -= 1
        return self.dp[self.m]

class BackPack2(object):
    """"""
    def __init__(self, Ai, Vi, m):
        self.Ai = Ai
        self.Vi = Vi
        self.m = m

    def capacity(self):
        self.dp = [0] * (self.m+1)
        for i in range(len(self.Ai)):
            j = self.m
            while j > 0:
                if j >= self.Ai[i]:
                    self.dp[j] = max(self.dp[j], self.dp[j-self.Ai[i]]+self.Vi[i])
                j -= 1
        return self.dp[self.m]



if __name__=='__main__':
    # test 1.1
    bp = BackPack([2, 3, 5, 7], 11)
    print (bp.capacity())

    # test 1.2
    bp2 = BackPack([2, 3, 5, 7], 12)
    print (bp2.capacity())

    # test 1.3
    solution3 = 5000
    test_arr = [828,125,740,724,983,321,773,678,841,842,875,377,674,144,340,467,625,916,463,922,255,662,692,123,778,766,254,559,480,483,904,60,305,966,872,935,626,691,832,998,508,657,215,162,858,179,869,674,452,158,520,138,847,452,764,995,600,568,92,496,533,404,186,345,304,420,181,73,547,281,374,376,454,438,553,929,140,298,451,674,91,531,685,862,446,262,477,573,627,624,814,103,294,388]
    bp3 = BackPack(test_arr, solution3)
    print (bp3.capacity() == solution3)


    # test 2.1
    solution2 = 9
    bp21 = BackPack2([2,3,4,7], [1,5,2,4], 10)
    print (bp21.capacity() == solution2)

