

# 0 
#Write a function called "angry_file_finder" that accepts a
#filename as a parameter. The function should open the file,
#read it, and return True if the file contains "!" on every
#line. Otherwise the function should return False. 

def angry_file_finder(filename):
	file_opened = open(filename, "r")
	for line in file_opened:
		if not "!" in line:
			file_opened.close()
			return False
	return True


# 1
#One of the reasons that filetypes work is that everyone 
#agrees how they are structured. A ".png" file, for example, 
#always contains "PNG" in the first four characters to 
#assure the viewer that the file is actually a png. If these
#standards were not set, it would be hard to write programs 
#that know how to open and read the file. 
#
#Let’s define a new filetype called ".cs1301". 
#In this file, every line should be structured like so:
#
#number assignment_name grade total weight
#
#In this file, each component will meet the following
#description:
#
# - number: an integer-like value of the assignment number 
#
# - assignment_name: a string value of the assignment name
#
# - grade: an integer-like value of a student’s grade
#
# - total: an integer-like value of the total possible
#   number of points
#
# - weight: a float-like value ranging from 0 to 1 
#   representing the percent of the student’s grade this 
#   assignment is worth. All the weights should add up to 1.
#
#Each component should be separated with exactly one space. 
#A good sample file is available to view as 
#"sample.cs1301".
#
#Write a function called format_checker that accepts a 
#filename and returns True if the file contents accurately 
#conform to the described format. Otherwise the function 
#should return False. In other words, it should return True
#if:
#
# - Each line has five elements separated by spaces, AND
# - The first, third, and fourth elements are integers, AND
# - The fifth element is a decimal number, AND
# - All the fifth elements add to 1.
#
#You can make changes to test.cs1301 to test your function,
#or test it with sample.cs1301. Right now, running it on
#sample.cs1301 should return True, and on test.cs1301
#should return False.
#
#Hint 1: .split() will likely help separate each line into 
#its components.
#Hint 2: .split() returns a list. So, if you were to do
#something like say splitLine = line.split(), then
#splitLine[0] would give the first item, splitLine[1] would
#give the second item, etc.
#Hint 3: Remember, the moment you find a single place where
#the file doesn't conform to the structure, then you can
#return False. Then, if you reach the end of the file, then
#it must be correct and you should return True.

def format_checker(filename):
	file_opened = open(filename, 'r')
	weight_sum = 0
	for line in file_opened:
		items = line.rstrip().split(" ")
		print(items)
		if not (len(items)==5 and items[0].isdigit() and
			items[2].isdigit() and items[3].isdigit() and
			items[4].replace('.', '').isdigit()):
			file_opened.close()
			return False
		weight_sum += float(items[4])
	if weight_sum != 1.0:
		file_opened.close()
		return False
	else:
		file_opened.close()
		return True

# 2
#Write a function called "reader" that reads in a ".cs1301" 
#file described in the previous problem. The function should 
#return a list of tuples representing the lines in the file like so:
#
#[(line_1_number, line_1_assignment_name, line_1_grade, line_1_total, line_1_weight), 
#(line_2_number, line_2_assignment_name, line_2_grade, line_2_total, line_2_weight)]
#
#All items should be of type int except for the name (string) 
#and the weight (float). You can assume the file will be in the 
#proper format.
#
#Hint: Although you could use readlines() to read in all
#the lines at once, they would all be strings, not a list.
#You still need to go line-by-line and convert each string
#to a list.

def reader(filename):
	file_opened = open(filename, 'r')
	lines = file_opened.readlines()
	line_list = []
	for line in lines:
		items = line.rstrip().split(' ')
		line_list.append((int(items[0]), items[1], int(items[2]), int(items[3]), float(items[4])))
	file_opened.close()
	return line_list

# 3
#Write a function called write_1301 which will write a file
#in the format described in Coding Problem 4.4.2. The
#sample.cs1301 file has been included to refresh your 
#memory. Your function should accept two arguments:
#A string of a filename to write to, and a list of tuples. 
#You can assume that each tuple will have the following 
#format:
#
#(int, str, int, int, float)
#
#Each tuple will represent a line in the file, and each
#item in the tuple should correspond to the 
#assignment number, the assignment name, the student's 
#grade, the total possible number of points, and the 
#assignment weight respectively. 

def write_1301(filename, tuple_list):
	file_to_write = open(filename, 'w')
	for t in tuple_list:
		line = str(t[0]) + ' ' + t[1] + ' ' + str(t[2]) + ' ' + str(t[3]) + ' ' + str(t[4]) + '\n'
		file_to_write.write(line)
	file_to_write.close()
	return file_to_write

# 4
#Write a function called get_grade that will read a
#given .cs1301 file and return the student's grade.
#To do this, we would recommend you first pass the
#filename to your previously-written reader() function,
#then use the list that it returns to do your
#calculations. You may assume the file is well-formed.
#
#A student's grade should be 100 times the sum of each
#individual assignment's grade divided by its total,
#multiplied by its weight. So, if the .cs1301 just had
#these two lines:
#
# 1 exam_1 80 100 0.6
# 2 exam_2 30 50 0.4
#
#Then the result would be 72:
#
# (80 / 100) * 0.6 + (30 / 50) * 0.4 = 0.72 * 100 = 72

def get_grade(filename):
	file_opened = open(filename, 'r')
	total = 0
	for line in file_opened:
		items = line.rstrip().split(' ')
		grade = int(100*float(items[4]) * (1.0*int(items[2])/int(items[3])))
		total += grade
	file_opened.close()
	return total



# 5
#We've given you a file called "top500.txt" which contains
#the name and lifetime gross revenue of the top 500
#films at the time this question was written. 
#
#Each line of the given file is formatted like so:
# <name>\t<gross revenue in dollars>
#
#In other words, you should call .split("\t") to split
#the line into the movie name (the first item) and the
#gross (the second item).
#
#Unfortunately, these movies are not in order. Write a 
#function called "sort_films" that accepts two parameters:
#a string of a source filename and a string of a
#destination filename. Your function should open the
#source file and sort the contents from greatest gross
#revenue to least gross revenue, and write the sorted
#contents to the destination filename. You may assume the
#source file is correctly formatted.

def sort_films(filename, destination_filename):
	file_opened = open(filename, 'r')
	cont_dict = {}
	rev_list = []
	for line in file_opened:
		items = line.rstrip().split('\t')
		cont_dict[int(items[1])] = items[0]
		rev_list.append(int(items[1]))
	file_opened.close()
	rev_list_sorted = sorted(rev_list, reverse=True)
	destination_file_open = open(destination_filename, 'w')
	for rev in rev_list_sorted:
		line_in = cont_dict[rev] + '\t' + str(rev) + '\n'
		destination_file_open.write(line_in)
	destination_file_open.close()
	

# 6
#Write a function, called luckySevens, that takes in one
#parameter, a list of integers, and returns True if the list
#has three '7's  in a row and False if the list doesn't.
#
#For example:
#
#  luckySevens([4, 7, 8, 2, 7, 7, 7, 3, 4]) -> True
#  luckySevens([4, 7, 7, 2, 8, 3, 7, 4, 3]) -> False
#
#Hint: As soon as you find one instance of three sevens, you
#could go ahead and return True -- you only have to find it
#once for it to be True! Then, if you get to the end of the
#function and haven't returned True yet, then you might
#want to return False.

def luckySevens(li):
	which_seven = 0
	i = 0
	while i < len(li):
		if li[i] == 7:
			which_seven += 1
			if which_seven == 3:
				return True
			i += 1
		else:
			which_seven = 0
			i += 1
	return False


# 7
#Write a function called attendanceCheck. attendanceCheck
#should have two parameters: roster and present. Both roster
#and present will be lists of strings. Return a list of all
#strings in the list roster that are not in the list
#present. In other words, if roster is a list of students
#enrolled in a class and present is a list of students in
#class today, return a list of students that are absent.
#
#You may assume that every item in each list will be a
#string. You may also assume that every name in the list
#present will be in the list roster. If no students are
#absent, return an empty list.

def attendanceCheck(roster, present):
	not_present = []
	for rost in roster:
		if rost not in present:
			not_present.append(rost)
	return not_present


# 8
#Write a function called gradeScantron. gradeScantron should
#take as input two lists: answers and key. Each list contain
#strings. Each string will be only one letter, a character
#from A to E. Your code should return an integer
#representing for how many indices in the lists the value of
#answers is equal to the value of key. In other words, if we
#assume that the list answers is the ordered list of answers
#to a test, and the list key is the ordered list of correct
#answers, the function should return how many answers they
#got right.
#
#If the lists do not have the same number of items, return
#-1 to indicate that the answer key did not belong to the
#same test as the student's answers.

def gradeScantron(answers, key):
	if len(answers) != len(key):
		return -1
	grade = 0
	for i in range(len(answers)):
		if answers[i] == key[i]:
			grade += 1
	return grade


# 9
#Write a function called solveRightTriangle. The function
#solveRightTriangle should have three parameters: opposite, 
#adjacent, and useDegrees. opposite and adjacent will be
#floats, and useDegrees will be a boolean. useDegrees
#should be a keyword parameter, and it should have a
#default value of False.
#
#The function should return a tuple containing the
#hypotenuse and angle of the right triangle (in that order).
#If useDegrees is False, the angle should be in radians. If
#useDegrees is True, the angle should be in degrees.
#
#Remember, the formula for the hypotenuse of a right
#triangle is the square root of the sum of the squared side
#lengths. You can find arctan using math.atan(), passing in
#the quotient of the opposite and adjacent as the argument.
#By default, math.atan() returns the angle in radians; you
#can pass that angle as an argument into the math.degrees()
#method to convert it to degrees.

def solveRightTriangle(opposite, adjacent, useDegrees=False):
	import math
	angle = math.atan(opposite/adjacent)
	hypotenuse = math.sqrt(opposite**2 + adjacent**2)
	if useDegrees:
		angle = math.degrees(angle)
	return (hypotenuse, angle)


# 10
#Write a function called findMaxSales. findMaxSales will
#have one parameter: a list of tuples. Each tuple in the
#list will have two items: a string and an integer. The
#string will represent the name of a movie, and the integer
#will represent that movie's total ticket sales (in millions
#of dollars).
#
#The function should return the movie from the list that
#had the most sales. Return only the movie name, not the
#full tuple.

def findMaxSales(movies):
	maxRev = 0
	best_seller = ""
	for movie in movies:
		if movie[1] > maxRev:
			best_seller = movie[0]
			maxRev = movie[1]
	return best_seller

# 11
#Write a function called stringSplitter tat replicates the
#function of the string type's split() method, assuming that
#we're splitting at spaces. stringSplitter should take as
#input a string, and return as output a list of the
#individual words from the string, assuming that words were
#divided by spaces. The spaces themselves should not be in
#the list that your function returns.
#
#You may assume that the input will have no punctuation,
#and that there will never be more than one space in a row.
#
#You may not use Python's built-in split() method.
#
#For example:
#
#  stringSplitter("Hello world") -> ['Hello', 'world']

def stringSplitter(strings):
	string = ""
	s_list = []
	i = 0
	while i < len(strings):
		if strings[i] != " ":
			string += strings[i]
		else:
			s_list.append(string)
			string = ""
		i += 1
		if i == len(strings):
			s_list.append(string)
	return s_list



def main():

	# test 0
	print(angry_file_finder("AngryFileFinderInput.txt"))
	# test 1
	print(format_checker("sample.cs1301"))
	print(format_checker("test.cs1301"))
	# test 2
	print(reader("sample.cs1301"))
	# test 3
	tuple1 = (1, "exam_1", 90, 100, 0.6)
	tuple2 = (2, "exam_2", 95, 100, 0.4)
	tupleList = [tuple1, tuple2]
	write_1301("test.cs1301", tupleList)
	#test 4
	print(get_grade("sample.cs1301")) # result: 92.9337
	# test 5
	sort_films("top500.txt", "top500result.txt")
	print("Done!")
	# test 6
	print(luckySevens([4, 7, 8, 2, 7, 7, 7, 3, 4]))
	print(luckySevens([4, 7, 7, 2, 8, 3, 7, 4, 3]))
	# test 7
	testRoster = ['Jessica', 'Nick', 'Winston', 'Schmidt', 'Cece', 'Ferguson']
	testPresent = ['Nick', 'Cece', 'Schmidt', 'Jessica']
	print(attendanceCheck(testRoster, testPresent))
	# test 8
	testAnswers = ["A", "B", "B", "A", "D", "A", "B", "A", "E"]
	testKey = ["A", "B", "B", "A", "D", "E", "B", "A", "D"]
	print(gradeScantron(testAnswers, testKey))
	# test 9
	#(5.0, 0.6435).
	print(solveRightTriangle(3.0, 4.0))
	# 10
	#correctly, the output presently should be: Rogue One.
	movieList = [("Finding Dory", 486), ("Captain America: Civil War", 408), ("Deadpool", 363), ("Zootopia", 341), ("Rogue One", 529), ("The Secret Life of Pets", 368), ("Batman v Superman", 330), ("Sing", 268), ("Suicide Squad", 325), ("The Jungle Book", 364)]
	print(findMaxSales(movieList))
	# test 11
	print(stringSplitter("Hello world"))



if __name__=="__main__":
	main()