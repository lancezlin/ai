

# 1
#We've started a recursive function below called strLength
#that should take in one string parameter, myStr, and
#returns its length. However, you may not use Python's
#built-in len function.
#

def strLength(mystr):
	if mystr == "":
		return 0
	else:
		return strLength(mystr[:-1]) + 1

# 2
#Remember that Fibonacci's sequence is a sequence of numbers
#where every number is the sum of the previous two numbers.
#
#For this problem, implement Fibonacci recursively, with a
#twist! Imagine that we want to create a new number sequence
#called Fibonacci-3. In Fibonacci-3, each number in the
#sequence is the sum of the previous three numbers. The
#sequence will start with three 1s, so the fourth Fibonacci-3
#number would be 3 (1+1+1), the fifth would be 5 (1+1+3),
#the sixth would be 9 (1+3+5), the seventh would be 17
#(3+5+9), etc.

def fib3(n):
	if n <= 3:
		return 1
	else:
		return fib3(n-1) + fib3(n-2) + fib3(n-3)

# 3
#Implement insertion sort below.
#
#Name your function 'insertion'. insertion should take as
#input a list, and return as output a sorted list. Note that
#even though technically a sorting method does not have to
#return the sorted list, yours should.
#
def insertion(l):
	n = len(l)
	for i in range(n-1):
		j = i + 1
		while j > 0:
			if l[j] < l[j-1]:
				tmp_l = l[j-1]
				l[j-1] = l[j]
				l[j] = tmp_l
			j -= 1
	return l

# 4
#Change it such that instead of sorting from
#lowest to highest, it sorts from highest to lowest.
#
#Name your function merge_sort(). For example, if you call
#merge_sort([5, 3, 1, 2, 4]), you would get [5, 4, 3, 2, 1].
#
def merge_sort(l):
	n = len(l)
	mid = int(n/2)

	if n == 1: return l
	lo = merge_sort(l[0:mid])
	hi = merge_sort(l[mid:n])
#	return _merge(lo, hi)
#def _merge(lo, hi):
	new_l = []
	while len(lo) > 0 and len(hi) > 0:
		if lo[0] > hi[0]:
			new_l.append(lo[0])
			del lo[0]
		else:
			new_l.append(hi[0])
			del hi[0]
	if len(lo) > 0:
		for i in range(len(lo)):
			new_l.append(lo[i])
	if len(hi) > 0:
		for j in range(len(hi)):
			new_l.append(hi[j])
	#print(new_l)
	return new_l


# 5 
#Write a function called string_search() that takes two
#parameters, a list of strings, and a string. This function
#should return  a list of all the indices at which the
#string is found within the list.
#
#You may assume that you do not need to search inside the
#items in the list; for examples:
#
#  string_search(["bob", "burgers", "tina", "bob"], "bob")
#      -> [0,3]
#  string_search(["bob", "burgers", "tina", "bob"], "bae")
#      -> []
#  string_search(["bob", "bobby", "bob"])
#      -> [0, 2]
#
#Use a linear search algorithm to achieve this. Do not
#use the list method index.
#
def string_search(strList, string):
	str_index = []
	i = 0
	for strL in strList:
		if strL == string:
			str_index.append(i)
		i += 1
	return str_index


# 6
#Recall in Worked Example 5.2.5 that we showed you the code
#for two versions of binary_search: one using recursion, one
#using loops.
#
#In this problem, we want to implement a new version of
#binary_search, called binary_year_search. binary_year_search
#will take in two parameters: a list of instances of Date,
#and a year as an integer. It will return True if any date
#in the list occurred within that year, False if not.
#
#For example, imagine if listOfDates had three instances of
#date: one for January 1st 2016, one for January 1st 2017,
#and one for January 1st 2018. Then:
#
#  binary_year_search(listOfDates, 2016) -> True
#  binary_year_search(listOfDates, 2015) -> False
#
#You should not assume that the list is pre-sorted, but you
#should know that the sort() method works on lists of dates.
#
def binary_year_search(listOfDates, yr):
	from datetime import date
	listOfDates.sort() # ascending order

	n, mid = len(listOfDates), int(len(listOfDates)/2)
	if len(listOfDates)==1:
		if listOfDates[mid].year == yr: 
			return True
		else:
			return False
	if listOfDates[mid].year == yr:
		return True
	elif listOfDates[mid].year > yr:
		return binary_year_search(listOfDates[0:mid], yr)
	else:
		return binary_year_search(listOfDates[mid:n], yr)

# 7
# find the max sum from the sub array

def sub_array_max_sum(arr):
	n = len(arr)
	max_sum = 0
	for i in range(n):
		j, sum = i, 0
		while j < n:
			sum += arr[j]
			if sum > max_sum:
				max_sum = sum
			j += 1
	return max_sum



# Testing:
def main():
	# test 1:
	print(strLength("asdfghjkl")) #9

	# test 2:
	print(fib3(7)) #17

	# test 3:
	print(insertion([2,3,1,7,4,5,6]))

	# test 4
	print(merge_sort([2,3,1,7,4,5,6]))

	# test 5
	print(string_search(["bob", "burgers", "tina", "bob"], "bob"))

	# test 6
	from datetime import date
	listOfDates = [date(2016, 11, 26), date(2014, 11, 29), 
               date(2008, 11, 29), date(2000, 11, 25), 
               date(1999, 11, 27), date(1998, 11, 28), 
               date(1990, 12, 1), date(1989, 12, 2), 
               date(1985, 11, 30)]
	print(binary_year_search(listOfDates, 2016))
	print(binary_year_search(listOfDates, 2007))

	# test 7
	print(sub_array_max_sum([-2, 3, 5, -3, 2, -5, 9, 2, -1]))
# Run Main function
if __name__=="__main__":
	main()