# /usr/bin/env python

#1
#Write a function called modifyDict. modifyDict takes one
#parameter, a dictionary. The dictionary's keys are people's
#last names, and the dictionary's values are people's first
#names. For example, the key "Joyner" would have the value
#"David".
#
#modifyDict should delete any key-value pair for which the
#key's first letter is not capitalized. For example, the
#key-value pair "joyner"-"David" would be deleted, but the
#key-value pair "Joyner"-"david" would not be deleted. Then,
#return the modified dictionary.
#
#Remember, the keyword del deletes items from lists and
#dictionaries. For example, to remove the key "key!" from
#the dictionary myDict, you would write: del myDict["key!"]
#Or, if the key was the variable myKey, you would write:
#del myDict[myKey]
#
#Hint: If you try to delete items from the dictionary while
#looping through the dictionary, you'll run into problems!
#We should never change the number if items in a list or
#dictionary while looping through those items. Think about
#what you could do to keep track of which keys should be
#deleted so you can delete them after the loop is done.
#
#Hint 2: To check if the first letter of a string is a
#capital letter, use string[0].isupper().
#
def modifyDict(mydict):
	last_names = mydict.keys()
	remain_keys = []
	for ln in last_names:
		if ln[0].isupper():
			remain_keys.append(ln)
	new_dict = {key:mydict[key] for key in remain_keys}
	return new_dict

#2
#Recall in coding problem 4.4.3 that you wrote a function
#called "reader" that read a .cs1301 file and returned a
#list of lists.
#
#Let's revise that problem. Instead of a list of lists,
#that's return a dictionary of dictionaries.
#
#Write a function called "reader" that takes one parameter,
#a filename as a string corresponding to a .cs1301 file,
#and reads in that .cs1301 file.
#
#Each line of the .cs1301 file will have five items, each
#separated by a space: the first, third, and fourth will
#represent integers, the second will be a string, and the
#fifth will represent a float. (Note: when reading the
#file, these will all be strings; you can assume each of
#these strings can be converted to the corresponding data
#type, however.)
#
#The function should return a dictionary of dictionaries
#representing the file contents.
#
#The keys of the top-level dictionary should be the
#assignment names. Then, the value for each of those keys
#should be a dictionary with four keys: "number", "grade",
#"total", and "weight". The values corresponding to each of
#those four keys should be the values from the file,
#converted to the corresponding data types (ints or floats).
#
#For example, if the input file read:
#
# 1 exam_1 90 100 0.6
# 2 exam_2 95 100 0.4
#
#Then reader would return this dictionary of dictionaries:
#
# {"exam_1": {"number": 1, "grade": 90, "total": 100, "weight": 0.6},
#  "exam_2": {"number": 2, "grade": 95, "total": 100, "weight": 0.4}}
#
#Hint: Although the end result is pretty different, this
#should only dictate a minor change to your original
#Problem 4.4.3 code!
def reader(filename):
	opened = open(filename, 'r')
	result = {}
	for line in opened:
		items = line.split(' ')
		result[items[1]] = {'number':int(items[0]),
							'grade':int(items[2]),
							'total':int(items[3]),
							'weight':float(items[4])}
	opened.close()
	return result

#3
#This is a challenging one! We don't expect everyone to be
#able to do it, but it's a good chance to test how far
#you've come!
#
#Write a function called stars that takes in two
#dictionaries:
#
# - movies: a dictionary where the keys are movie titles and
#   the values are the main actor/actress in the movie. For
#   example, movies["Hunger Games"] = "Jennifer Lawrence".
# - tvshows: a dictionary where the keys are TV show titles
#   and the values are the main actor/actress in that show.
#   For example, tvshows["Community"] = "Joel McHale".
#
#The function stars should return a new dictionary. The keys
#of the new dictionary should be the actors'/actresses'
#names, and the values for each key should be the list of
#shows and movies in which that actor/actress has appeared.
#
#For example:
#
# movies = {"Fences": "Viola Davis", "Hick": "Blake Lively",
#           "The Hunger Games": "Jennifer Lawrence"}
# tvshows = {"How to Get Away with Murder": "Viola Davis",
#            "Gossip Girl": "Blake Lively",
#            "The Office": "Steve Carrell"}
# stars(movies, tvshows) ->
#  {"Viola Davis": ["Fences", "How to Get Away with Murder"],
#   "Steve Carrell": ["The Office"], "Jennifer Lawrence":
#   ["The Hunger Games"], "Blake Lively": ["Hick",
#   "Gossip Girl"]}

def stars(movies, tvshows):
	result = {}
	for movie_actor in movies.items():
		if not movie_actor[1] in result:
			result[movie_actor[1]] = [movie_actor[0]]
		else:
			result[movie_actor[1]].append(movie_actor[0])

	for tv_actor in tvshows.items():
		if not tv_actor[1] in result:
			result[tv_actor[1]] = [tv_actor[0]]
		else:
			result[tv_actor[1]].append(tv_actor[0])
	return result


#We've given you a class called "Song" that represents
#some basic information about a song. We also wrote a 
#class called "Artist" which contains some basic 
#information about an artist. Your job is to do the
#following:
#
# 1. Create a song object with the following attributes:
#       name = "You Belong With Me"
#       album = "Fearless"
#       year = 2008
#       artist.name = "Taylor Swift"
#       artist.label = "Big Machine Records, LLC"
# Store this song object in a variable named "song_1"
# Hint: You'll first need to make an Artist object
#
# 2. Create another song object with the following 
#    attributes:
#       name = "All Too Well"
#       album = "Red"
#       year = 2012
#       artist = the same artist instance as song_1
# Store this song object in a variable named "song_2"
#
# 3. Finally, create another song object with the following
#    attributes:
#       name = "Up We Go"
#       album = "Midnight Machines"
#       year = 2016
#       artist.name = "LIGHTS"
#       artist.label = "Warner Bros. Records Inc."
# Store this song object in a variable named "song_3"
#
#When your code is done running, there should exist three
#variables: song_1, song_2, and song_3, according to the
#requirements above.

class Artist:
	def __init__(self, name, label):
		self.name = name
		self.label = label

class Song:
	def __init__(self, name, album, year, artist):
		self.name = name
		self.album = album
		self.year = year
		self.artist = artist


#Classes can also have references to other instances of
#themselves. Consider this Person class, for example, 
#that allows for an instance of a father and mother
#to be given in the constructor.
#
#Create 3 instances of this class. The first should have 
#the name "Mr. Burdell" with an age of 53. The second
#instance should have a name of "Mrs. Burdell" with an age
#of 53 as well. Finally, make an instance with the name of
#"George P. Burdell" with an age of 25. This final instance
#should also have the father attribute set to the instance 
#of Mr. Burdell, and the mother attribute set to the 
#instance of Mrs. Burdell. Finally, store the instance of 
#George P. Burdell in a variable called george_p. (It does
#not matter what variable names you use for Mr. and Mrs.
#Burdell.)

class Person:
	def __init__(self, name, age, father=None, mother=None):
		self.name = name
		self.age = age
		self.father = father
		self.mother = mother

#Write your code here!
p1 = Person("Mr. Burdell", 53)
p2 = Person("Mrs. Burdell", 53)
george_p = Person("George P. Burdell", 25, p1, p2)

#The code below will let you test your code. It isn't used
#for grading, so feel free to modify it. As written, it
#should print George P. Burdell, Mrs. Burdell, and Mr.
#Burdell each on a separate line.
print(george_p.name)
print(george_p.mother.name)
print(george_p.father.name)

#Here's a long one -- you can do it!
#
#Rewrite the following class so that it uses getters and
#setters for all three variables (title, description,
#completed). The getters should be called: getTitle,
#getDescription,  getCompleted. The setters should be
#called: setTitle, setDescription, setCompleted.
#
#Whenever a getter is called, the method should print
#"<attribute name> accessed". For example, when calling
#getTitle(), the program should print "title accessed".
#
#Whenever a setter is called, the method should print
#"<attribute name> changed". For example, when calling
#the method setCompleted(True), the program should print
#"completed changed".
#
#In addition, the setter should check to make sure that
#the new value is the correct type: title and description
#should always be of type str, and completed should always
#be of type bool. If the value is not the right type,
#print "invalid value" in addition to "<attribute name>
#changed", and set the value of the corresponding
#attribute to None (the keyword, not the string "None").
#
#Note that when assigning variables their initial values
#inside the constructor, you should use the getters and
#setters, not modify the variables directly.
#
#To summarize (and give a to-do list):
# - Create getters and setters for each variable.
# - Print a log statement when the getters and setters
#   are called.
# - Check the type of the new value inside the setters,
#   and print an error if it's the wrong type.
# - Modify the constructor body to use the getters and/or
#   setters instead of assigning values directly.
#
#See the bottom of the code for hints!


class TodoItem:
	def __init__(self, title, description, completed=False):
		self.setTitle(title)
		self.setDescription(description)
		self.setCompleted(completed)

	def setTitle(self, title):
		print("title changed")
		if type(title)==str:
			
			self.title = title
		else:
			print("invalid value")
			self.title = None
	def setDescription(self, description):
		print("description changed")
		if type(description)==str:
			
			self.description = description
		else:
			print("invalid value")
			self.description = None
	def setCompleted(self, completed):
		print("completed changed")
		if type(completed)==bool:
			
			self.completed = completed
		else:
			print("invalid value")
			self.completed = None

	def getTitle(self):
		print("title accessed")
		return self.title
	def getDescription(self):
		print("description accessed")
		return self.description
	def getCompleted(self):
		print("completed accessed")
		return self.completed



#Hint: You will need to change the body of the constructor
#and add six new methods to this class, but you should not
#need to change the header of the class or the constructor.
#
#Hint 2: You can check to see if a variable is a string by
#checking the logical expression type(var) == str, where
#var is the variable you're checking. For integers, use
#int instead of str. For floats, use float. For booleans,
#use bool.
#
#Hint 3: Remember to put self before any instance variables
#or methods you're trying to access. For example, to access
#the method setTitle from within the constructor, you would
#need to write self.setTitle().

#The Fibonacci sequence is a number sequence where each
#number is the sum of the previous two numbers. The first
#two numbers are defined as 0 and 1, so the third number is
#1 (0 + 1 = 1), the fourth number is 2 (1 + 1 = 2), the
#fifth number is 3 (1 + 2 = 3), the sixth number is 5
#(2 + 3 = 5), and so on.
#
#Below we've started a class called FibSeq. At any time,
#FibSeq holds two values from the Fibonacci sequence:
#back1 and back2.
#
#Create a new method inside FibSeq called next_number. The
#next_number method should:
#
# - Calculate and return the next number in the sequence,
#   based on the previous 2.
# - Update back2 with the former value of back1, and update
#   back1 with the new next item in the sequence.
#
#This means that consecutive calls to next_number should
#yield each consecutive number from the Fibonacci sequence.
#Calling next_number 5 times would print 1, 2, 3, 5, and 8.


class FibSeq:
	def __init__(self):
		self.back1 = 1
		self.back2 = 0

	def next_number(self):
		nxt = self.back1 + self.back2
		self.back2 = self.back1
		self.back1 = nxt
		return nxt
	 
#The code below will test your method. It's not used for
#grading, so feel free to change it. As written, it should
#print 1, 2, 3, 5, and 8.
newFib = FibSeq()
print(newFib.next_number())
print(newFib.next_number())
print(newFib.next_number())
print(newFib.next_number())
print(newFib.next_number())



def main():
	# test 1
	myDict = {'Joshua':'Diaddigo', 'joyner':'David', 'Elliott':'jackie', 'murrell':'marguerite'}
	print(modifyDict(myDict))

	# 3
	movies = {"Fences": "Viola Davis", "Hick": "Blake Lively",
		   "The Hunger Games": "Jennifer Lawrence"}
	tvshows = {"How to Get Away with Murder": "Viola Davis",
			"Gossip Girl": "Blake Lively",
			"The Office": "Steve Carrell"}
	print(stars(movies, tvshows))

	art_1 = Artist("Taylor Swift", "Big Machine Records, LLC")
	art_2 = Artist("LIGHTS", "Warner Bros. Records Inc.")
	song_1 = Song("You Belong With Me", "Fearless", 2008, art_1)
	song_2 = Song("All Too Well", "Red", 2012, art_1)
	song_3 = Song("Up We Go", "Midnight Machines", 2016, art_2)

if __name__=="__main__":
	main()
