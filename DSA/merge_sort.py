#Let's implement Mergesort! This is a complex problem
#because it applies recursion to sorting algorithms, but
#it's also by far the most efficient sorting algorithm we'll
#cover.

#First, we need a function definition: MergeSort should take
#as input one list.

def mergesort(lst):
	
	#Then, what does it do? mergesort should recursively
	#run mergesort on the left and right sides of lst until
	#it's given a list only one item. So, if lst has only
	#one item, we should just return that one-item list.
	
	if len(lst) <= 1:
		return lst
	
	#Otherwise, we should call mergesort separately on the
	#left and right sides. Since each successive call to
	#mergesort sends half as many items, we're guaranteed
	#to eventually send it a list with only one item, at
	#which point we'll stop calling mergesort again.
	else:

		#Floor division on the length of the list will
		#find us the index of the middle value.
		midpoint = len(lst) // 2

		#lst[:midpoint] will get the left side of the
		#list based on list slicing syntax. So, we want
		#to sort the left side of the list alone and
		#assign the result to the new smaller list left.
		left = mergesort(lst[:midpoint])

		#And same for the right side.
		right = mergesort(lst[midpoint:])

		#So, left and right now hold sorted lists of
		#each half of the original list. They might
		#each have only one item, or they could each
		#have several items.

		#Now we want to compare the first items in each
		#list one-by-one, adding the smaller to our new
		#result list until one list is completely empty.

		newlist = []
		while len(left) and len(right) > 0:

			#If the first number in left is lower, add
			#it to the new list and remove it from left
			if left[0] < right[0]:
				newlist.append(left[0])
				del left[0]

			#Otherwise, add the first number from right
			#to the new list and remove it from right
			else:
				newlist.append(right[0])
				del right[0]

		#When the while loop above is done, it means
		#one of the two lists is empty. Because both
		#lists were sorted, we can now add the remainder
		#of each list to the new list. The empty list
		#will have no items to add, and the non-empty
		#list will add its items in order.

		newlist.extend(left)
		newlist.extend(right)

		#newlist is now the sorted version of lst! So,
		#we can return it. If this was a recursive call
		#to mergesort, then this sends a sorted half-
		#list up the ladder. If this was the original
		#call, then this is the final sorted list.

		return newlist

#Let's try it out!
print(mergesort([2, 5, 3, 8, 6, 9, 1, 4, 7]))


#We've written the function, selectionSort, below. It takes
#in one list parameter, aList. Our version of selection sort
#involves finding the minimum value and moving it to an
#earlier spot in the list.
#
#However, some lines of code are blank. Complete these lines
#to complete the selectionSort function. You should only need
#to modify the section marked 'Write your code here!'

def selectionSort(aList):
	
	#For each index in the list...
	for i in range(len(aList)):
		
		#Assume first that current item is already correct...
		minIndex = i

		#For each index from i to the end...
		for j in range(i + 1, len(aList)):
			
			#Complete the reasoning of this conditional to
			#complete the algorithm! Remember, the goal is
			#to find the lowest item in the list between
			#index i and the end of the list, and store its
			#index in the variable minIndex.
			#
			#Write your code here!

		#Save the current minimum value since we're about
		#to delete it
		minValue = aList[minIndex]
		
		#Delete the minimum value from its current index
		del aList[minIndex]
		
		#Insert the minimum value at its new index
		aList.insert(i, minValue)
	
	#Return the resultant list
	return aList
	

#The code below will test your fix. It is not used for
#grading, so feel free to modify it. As written, it should
#print a sorted list.
print(selectionSort([5, 3, 1, 2, 4]))



#We've written the function, bubbleSort, below. It takes in
#one list parameter, lst. However, there are two problems in
#our current code:
# - There's a missing line
# - There's a semantic error (the code does not raise an
#   error message, but it does not perform correctly)
#
#Find and fix these problems! Note that you should only need
#to change or add code where explicitly indicated.
#
#Hint: If you're stuck, use an example input list and trace
#the code and how it modifies your list on paper. For
#example, try writing out what happens to the following list:
#
#  [34, 16, 2, 78, 4, 6, 1]

def bubbleSort(lst):
	#Set swapOccurred to True to guarantee the loop runs once
	swapOccurred = True
	
	#Run the loop as long as a swap occurred the previous time
	while swapOccurred:
		
		#Start off assuming a swap did not occur
		swapOccurred = False
		
		#For every item in the list except the last one...
		for i in range(len(lst) - 1):

			#If the item should swap with the next item...
			if lst[i] > lst[i + 1]:

				#Then, swap them! But these lines aren't
				#quite right: fix this code!
				temp = lst[i]
				lst[i] = lst[i + 1]
				lst[i + 1] = temp

				#One more line is needed here; add it!
				swapOccurred = True
	return lst

#The line below will test your code. If it works, a sorted
#list will be printed. This is not used for grading, so
#feel free to change it.
print(bubbleSort([5, 3, 1, 2, 4]))


#We've started a recursive function below called
#exponentCalc(). It takes in two integer parameters, base
#and expo. It should return the mathematical answer to
#base^expo. For example, exponentCalc(5, 3) should return
#125: 5^3 = 124.
#
#The code is almost done - we have our base case written.
#We know to stop recursing when we've reached the simplest
#form. When the exponent is 0, we return 1, because anything
#to the 0 power is 1. But we are missing our recursive call!
#
#Fill in the marked line with the recursive call necessary
#to complete the function. Do not use the double-asterisk
#operator for exponentiation. Do not use any loops.
#
#Hint: Notice the similarity between exponentiation and
#factorial:
#  4! = 4! = 4 * 3!, 3! = 3 * 2!, 2! = 2 * 1
#  2^4 = 2 * 2^3, 2^3 = 2 * 2^2, 2^2 = 2 * 2^1, 2^1 = 2 * 2^0

def exponentCalc(base, expo):
	if expo == 0:
		return 1
	else:
		return exponentCalc(base, expo-1)#Complete this line!

#The code below will test your function. It isn't used for
#grading, so feel free to modify it. As originally written,
#it should print 125.
print(exponentCalc(5, 3))


