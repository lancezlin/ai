import pandas as pd
import numpy as np
import pandas as pd
from copy import deepcopy
from sklearn.linear_model import SGDClassifier
from sklearn.grid_search import GridSearchCV
import re
import os
from sklearn.feature_extraction.text import CountVectorizer
import nltk
from nltk.stem.porter import PorterStemmer
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import TfidfTransformer, TfidfVectorizer
from sklearn.feature_extraction.text import HashingVectorizer
import numpy as np
from sklearn.metrics import confusion_matrix
from nltk.stem import SnowballStemmer
from sklearn.cross_validation import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import OneHotEncoder, LabelEncoder, LabelBinarizer
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import confusion_matrix
#nltk.download('stopwords')

# configurations
data_path = "kenshoo_data-last_180_days-56a1f9d6b920-2017-06-02-20-44-48 (1).csv"

words_to_go = ['one', 'two', 'three', 'man', 'woman', 'men', 'women', 'br', 'four', 'five', \
				'also', 'still', 'yet', 'would', 'might', 'movi', 'film', 'boy', 'girl', \
				'think', 'seem', 'becaus', 'see', 'saw', 'may', 'watch', 'before', 'www', 'http', 'https'] # words with no meanings



grid_parameters = [{'loss':['hinge', 'log', 'perceptron'], 'penalty': ['l1', 'l2'], \
					'alpha':[0.0001, 0.001, 0.01, 0.1]}] # set up grid search parameters

def read_stopwords():
	stopwords = []
	with open('stopwords.txt', 'r') as stopword:
		lines = stopword.readlines()
		for line in lines:
			stopwords.append(line.strip())
	stopwords = set(stopwords)
	return stopwords

def text_cleanup(text, stopwords, words_to_go): 
	if len(text)==0: return None
	txt = re.sub('[^a-zA-Z]', ' ', text)
	txt = txt.lower()
	txt = txt.split()
	#ps = PorterStemmer()
	ps = SnowballStemmer('english')
	txt = [ps.stem(word) for word in txt]
	txt = [word for word in txt if not word in stopwords and not word in words_to_go and len(word)>2]
	txt = ' '.join(txt)

	return txt

def lyft_data_process(data_frame, col_num, stopwords, words_to_go):
 	'''
 	trying to clean up the string for columns
 	col_nums - a list of numbers
 	'''
	print ("preprocessing ...")

	

	textColumn = []
	texts = data_frame.iloc[:, col_num].values.astype(str)
	for t in texts:
		clean_t = text_cleanup(t, stopwords, words_to_go)
		textColumn.append(clean_t)

	df = pd.DataFrame(textColumn)
	col_name = 'text' + str(col_num)
	df.columns = [col_name]
	data_frame = pd.concat([data_frame, df], axis=1)
	return data_frame


	  
def get_y(data):
	return data.iloc[:,1].values

def imdb_data_cleanup(data):
	''' 
		removing stop words; punctations etc
		return: corpus as a list
	'''
	stopwords = []
	with open('stopwords.en.txt', 'r') as stopword:
		lines = stopword.readlines()
		for line in lines:
			stopwords.append(line.strip())
	corpus = []
	row, col = data.shape
	for i in range(row):
		review = text_cleanup(data['text'][i], stopwords)

		corpus.append(review)
		
	return corpus


def grid_search_init(model, parameters, X_train, y_train, cv=5):
	grid_search_cv = GridSearchCV(estimator = model,
								param_grid = parameters,
								scoring = 'accuracy',
								cv = cv)
	grid_search = grid_search_cv.fit(X_train, y_train)
	print(grid_search.best_params_)
	return grid_search.best_params_
	 


def SGD_builder(X, y, X_test, param):
	'''
		construct the SGD model
	'''
	sgd = SGDClassifier(random_state=0)
	bp = grid_search_init(sgd, param, X, y)
	best_model = SGDClassifier(loss=bp['loss'], penalty=bp['penalty'], alpha=bp['alpha'],random_state=0)
	best_model.fit(X, y)
	y_pred = best_model.predict(X_test)

	return y_pred, best_model


def multi_encoder(df, cols):
    result = pd.DataFrame()
    for col in cols:
        lb = LabelBinarizer()
        lb_trans = lb.fit_transform(df[col])
        result = pd.concat([result, pd.DataFrame(lb_trans, columns=lb.classes_)], axis = 1)
    return result

if __name__ == "__main__":

	# import the stopwords
	stopwords = read_stopwords()
	# read the data
	lyft_data = pd.read_csv(data_path)
	lyft_data['text'] = lyft_data['a.utm_campaign'].str.cat([lyft_data['a.adgroup'].astype(str), \
		lyft_data['a.utm_term'].astype(str), lyft_data['a.page'].astype(str)], sep = ' ')
	lyft_data = lyft_data.drop(lyft_data.columns[[3,4,5,8, 9, 10, 12, 13,15,19,21,22,23]], axis = 1)
	print((lyft_data.head(), lyft_data.shape))

	# clean up text in columns
	new_lyft_data = lyft_data_process(lyft_data, 12, stopwords, words_to_go).drop('text', axis=1)
	

	# data prep
	# 1. update the date
	new_lyft_data['af.ssn_bg_consent_date'].fillna(new_lyft_data['af.applied_date'], inplace=True)
	new_lyft_data['a.age_range'] = new_lyft_data['a.age_range'].fillna('age_unknown')
	new_lyft_data['a.gender'] = new_lyft_data['a.gender'].fillna('unknown')
	new_lyft_data['a.google_device'] = new_lyft_data['a.google_device'].fillna('device_unknown')
	new_lyft_data['a.region'] = new_lyft_data['a.region'].fillna('region_unknown')
	new_lyft_data['days_applied_to_bg'] = (pd.to_datetime(df1['af.ssn_bg_consent_date'])-pd.to_datetime(df1['af.applied_date']))/np.timedelta64(1, 'D')
	new_lyft_data['days_applied_to_bg'] = new_lyft_data['days_applied_to_bg'].fillna(0)

	#new_lyft_data.head()
	

	# 2. Encoder and Scaler
	encoder_columns = ['a.age_range', 'a.gender', 'a.google_device', 'a.region']
	encode_df = multi_encoder(new_lyft_data, encoder_columns)
	numeric_df = new_lyft_data.iloc[:, [-6,-5,-4,-1,-2]]
	mm_scaler = MinMaxScaler()
	numeric_df['days_applied_to_bg'] = mm_scaler.fit_transform(numeric_df['days_applied_to_bg'])
	X_df = pd

	# 3. split into 
	X = pd.concat([encode_df, numeric_df], axis = 1).values
	y = new_lyft_data.iloc[:, -3]
	X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.3, random_state = 0)
	X_train_corpus, X_test_corpus = [text for text in X_train[:, -1]], [text for text in X_test[:, -1]]
	
	# 4. unigram vectorize
	unigram = CountVectorizer(ngram_range = (1,1), stop_words = 'english', max_features=200)
	X_train_unigram = unigram.fit_transform(X_train_corpus).toarray()
	X_test_unigram = unigram.transform(X_test_corpus).toarray()
	X_train_final = np.concatenate((X_train[:, :-1], X_train_unigram), axis=1)
	X_test_final = np.concatenate((X_test[:, :-1], X_test_unigram), axis=1)

	# test out
	# 5. prediction
	y_pred_unigram, model = SGD_builder(X_train_final, y_train, X_test_final, grid_parameters)
	accuracy_unigram = accuracy_score(y_pred_unigram, y_test)
	conf_matrix = confusion_matrix(y_test, y_pred_unigram)
	
	# 6. results
	print(accuracy_unigram)
	print(conf_matrix)

