from random import randint
from BaseAI import BaseAI
from math import log
import time


directionVectors = (UP_VEC, DOWN_VEC, LEFT_VEC, RIGHT_VEC) = ((-1, 0), (1, 0), (0, -1), (0, 1))



def heuristic(grid):
	#start_time = time.time()
	# take in all the factors

	# support functions
	logTwo = lambda x: log(x)/log(2)
	n = grid.size
	max_tile = grid.getMaxTile()
	zeroCells = 0
	score = 0
	smoothyScore = 0
	UD_increase, UD_decrease, LR_increase, LR_decrease = 0, 0, 0, 0
#	islands = 0

#	def mark(value, i, j):
#		if (grid.getCellValue((i, j)) == value) and (tmp_grid[i][j] == 0) and (i>=0 and i<=3 and j>=0 and j<=3):
#			tmp_grid[i][j] = 1
#			for move in [0,1,2,3]:
#				vec = directionVectors[move]
#				mark(i+vec[0], j+vec[1], value)

	# corner function support
	if max_tile < 256:
		factor = 1.5
	else:
		factor = 2


	corner_index = {'left_up':(0, 0), 
					'left_down':(0, 3), 
					'right_up':(3, 0), 
					'right_down':(3, 3)}
	for corner in corner_index.keys():
		if grid.getCellValue(corner_index[corner]) == max_tile:
			cornerScore = factor * max_tile
		else :
			cornerScore = (-1) * factor * max_tile

	# for directions/islands/smoothy

	tmp_grid = [[0] * n for i in range(n)]
	for row in range(n):
		for col in range(n):
			if grid.getCellValue((row, col)) > 0:
				tmp_grid[row][col] = logTwo(grid.getCellValue((row, col)))
				score += logTwo(tmp_grid[row][col])
			else:
				tmp_grid[row][col] = 0
				zeroCells += 1

	for i in range(n):
		for j in range(n):
			if tmp_grid[i][j] != 0:
				if tmp_grid[i][3] != 0:
					smoothyScore += abs(logTwo(tmp_grid[i][j]) - logTwo(tmp_grid[i][3]))
				if tmp_grid[3][j] != 0:
					smoothyScore += abs(logTwo(tmp_grid[i][j]) - logTwo(tmp_grid[3][j]))


			if j < n - 1:
				if tmp_grid[i][j] > tmp_grid[i][j + 1]:
					LR_decrease -= tmp_grid[i][j] - tmp_grid[i][j + 1]
				else:
					LR_increase += tmp_grid[i][j] - tmp_grid[i][j + 1]
			if i < n - 1:
				if tmp_grid[i][j] > tmp_grid[i + 1][j]:
					UD_decrease -= tmp_grid[i][j] - tmp_grid[i + 1][j]
				else:
					UD_increase += tmp_grid[i][j] - tmp_grid[i + 1][j]
	directionScore = max(UD_increase, UD_decrease) + max(LR_decrease, LR_increase)

#	if not grid.getCellValue((i, j))==0 and tmp_grid[i][j]==0:
#		islands += 1
#		mark(i, j, grid.getCellValue((i, j)))

	#print("heuristic time: ", time.time() - start_time)
	return (score * 14 + zeroCells * 25 + max_tile * 10 + smoothyScore + cornerScore + directionScore*15)

		


class PlayerAI(BaseAI):
	"""docstring for PlayerAI"""
	def __init__(self):
		self.direction = 0

	def AlphaBetaMiniMax(self, grid, alpha, beta, depth, Player):
		available_moves = grid.getAvailableMoves()
		ideal_move = available_moves[randint(0, len(available_moves) - 1)] if available_moves else None
		best_score = 0
		if depth == 0: 
			#print (heuristic(grid))
			return (heuristic(grid), None)

		if Player: # max
			#print (depth, " : ", Player, " player.", "avail moves: ", available_moves)
			best_score = alpha
			if len(available_moves) > 0:
				for move in available_moves:
					tmp_grid = grid.clone()
					tmp_grid.move(move)
					result = self.AlphaBetaMiniMax(tmp_grid, alpha, beta, depth-1, False)
					if result[0] > best_score:
						best_score = result[0]
						ideal_move = move
					if best_score >= beta:
						#print ("max break:", best_score)
						break
					if alpha < best_score:
						alpha = best_score
				return (best_score, ideal_move)

		else: # min
			#print (depth, " : ", Player, " player.", "avail cells: ", len(grid.getAvailableCells()))
			best_score = beta
			for cell in grid.getAvailableCells():
				for cell_value in [2, 4]:
					tmp_grid = grid.clone()
					tmp_grid.setCellValue(cell, cell_value)
					result = self.AlphaBetaMiniMax(tmp_grid, alpha, beta, depth-1, True)
					tmp_grid.setCellValue(cell, 0)
					if result[0] < best_score:
						best_score = result[0]
					if best_score <= alpha:
						#print("min break: ", best_score)
						break
					if best_score < beta:
						beta = best_score #return(alpha, None)
			return(best_score, None)
		return(best_score, ideal_move)


	def depthChoice(self, grid):
		open_cells = len(grid.getAvailableCells())
		if open_cells > 3: 
			depth = 3
		#elif open_cells >= 2:
		#	depth = 3
		else: 
			depth = 4
		return self.AlphaBetaMiniMax(grid, float('-inf'), float('inf'), depth, True)[1]

	def getMove(self, grid):
		#return self.AlphaBetaMiniMax(grid, float('-inf'), float('inf'), 2, True)[1]
		return self.depthChoice(grid) 
		#return self.timeLimit(grid)
		#moves = grid.getAvailableMoves()
		#return moves[randint(0, len(moves) - 1)] if moves else None






		