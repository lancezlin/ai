import argparse
from copy import deepcopy
import sys



# defining constant
Chars = 'ABCDEFGHI'
N = 9

class CSP(object):
	"""docstring for CSP"""
	def __init__(self, csp):
		
		self.Vars = []
		self.Domains = []
		self.Constraints = []
		#self.Heuristics = Heuristics

		self.initialize(csp)


	def initialize(self, csp):
		# initialize data csp structure
		for i in range(N):
			for j in range(1, N + 1):
				self.Vars.append(Chars[i] + str(j))
				self.Domains.append(set(range(1, N + 1)))

		for k in range(len(csp)):
			#print (int(csp[k]))
			if int(csp[k]) != 0:
				self.Domains[k] = set([int(csp[k])])
		#print (self.Domains)

		self.arc_gen()

	def box_gen(self, r, c):
		# define the square constraints
		square = []

		for i in range(r, r+3):
			for j in range(c, c+3):
				var = Chars[i] + str(j+1)
				square.append(var)

		for varX in square:
			for varY in square:
				if varX[0]!=varY[0] or varX[1]!=varY[1]:
					insert_it = True
					for c in self.Constraints:
						if varX in c and varY in c:
							insert_it = False
					if insert_it:
						self.Constraints.append(set([varX, varY]))

	def arc_gen(self):
		# generate all arc consistency of rows and cols
		for r in [0, 3, 6]:
			for c in [0, 3, 6]:
				self.box_gen(r, c)

		for varX in self.Vars:
			for varY in self.Vars:
				if (varX[0]==varY[0] and varX[1]!=varY[1]) or\
					(varX[1]==varY[1] and varX[0]!=varY[0]):
					insert_it = True

					for c in self.Constraints:
						if varX in c and varY in c:
							insert_it = False

					if insert_it:
						self.Constraints.append(set([varX, varY]))



	def Done(self, assignments):
		# check if the assignment is complete or not
		i = 0
		for domain in self.Domains:
			if len(domain) > 1 and self.Vars[i] not in assignments:
				return False
			i += 1

		return True

	def successors(self, var):
		# get connected neighbors
		r, c = self.Vars.index(var) / N, self.Vars.index(var) % N
		suc = []

		for i in range(1, 10):
			r_var, c_var = Chars[r]+str(i), Chars[i-1]+str(c+1)
			# don't append itself
			if i!=c+1: suc.append(r_var)
			if i!=r+1: suc.append(c_var)

		for i in range((r/3)*3, (r/3)*3+3):
			for j in range((c/3)*3, (c/3)*3+3):
				box_var = Chars[i]+str(j+1)
				if box_var!=var and box_var not in suc:
					suc.append(box_var)

		return suc

	def consistency(self, var, value):
		# check consistency
		successors = self.successors(var)
		#print (var, ":", successors)
		for suc in successors:
			index = self.Vars.index(suc)
			domain = self.Domains[index]

			if len(domain)==1 and value in domain: 
				consistent = False

		return True

	def solvable(self):
		# check completeness
		for domain in self.Domains:
			if len(domain) > 1:
				return False
		return True

	def assign(self, assignments):
		# set assignment to csp domains
		for assignment in assignments:
			index = self.Vars.index(assignment)
			self.Domains[index] = set([assignments[assignment]])

	def Output(self):
		output = self.Domains
		output_format = ''
		for i in range(len(output)):
			it = output[i]
			output_format += str(it.pop())
		return output_format

# define the module

class SudokuSolver(object):
	"""docstring for SudokuSolver"""


	def Refined(self, csp, x, y):
		# update domain from one to another
		refined = False
		domain_x, domain_y = csp.Domains[csp.Vars.index(x)], csp.Domains[csp.Vars.index(y)]

		if len(domain_x)==1 and domain_x <= domain_y:
			domain_y = domain_y - domain_x
			csp.Domains[csp.Vars.index(y)] = domain_y
			refined = True

		elif len(domain_y)==1 and domain_y <= domain_x:
			domain_x = domain_x - domain_y
			csp.Domains[csp.Vars.index(x)] = domain_x
			refined = True

		return refined

	def MinRemainValue(self, csp, assignments):
		# return minimum remaining value of csp by sorting heuristics
		remaining = {}
		i = 0

		for domain in csp.Domains:
			if len(domain) > 1:
				remaining[csp.Vars[i]] = len(domain)
			i += 1

		remaining_sort = sorted(remaining, key = remaining.get)
		#print (remaining_sort)
		for var in remaining_sort:
			if var not in assignments: 
				return var

		return False

	def ForwardChecking(self, csp, var, value, assignments):
		# infer the successors' value
		successors = csp.successors(var)
		infers = {}
		#print (var, ":", successors)
		for suc in successors:
			domain = csp.Domains[csp.Vars.index(suc)]
			if value in domain and len(domain) > 1:
				domain = domain - set([value])
				index = csp.Vars.index(suc)
				csp.Domains[index] = domain

				if len(domain)==1 and suc not in assignments:
					infers[suc] = list(domain)[0]
			
			infer_values = infers.values()
			for infer in infer_values:
				if infer_values.count(infer) > 1: 
					return False

		return infers

	def AC3(self, csp):
		# arc consistency algo
		Q = [c for c in csp.Constraints]
		empty_q = lambda q: len(q) == 0

		while not empty_q(Q):
			c = Q.pop(0)
			x = c.pop()
			y = c.pop()
           
			if self.Refined(csp, x, y):
				if len(csp.Domains[csp.Vars.index(x)])==0 or \
					len(csp.Domains[csp.Vars.index(y)])==0: 
					return False

				if len(csp.Domains[csp.Vars.index(x)]) > 1:
					successors = csp.successors(x)
					successors.remove(y)
					for suc in successors:
						Q.append(set([x, suc]))

				elif len(csp.Domains[csp.Vars.index(y)]) > 1:
					successors = csp.successors(y)
					successors.remove(x)
					for suc in successors:
						Q.append(set([y, suc]))

		return True

	def BackTracking(self, csp, assignments):
		# implement backtracking with MRV
		if csp.Done(assignments): 
			print("Sudoku Solved with BackTracking!")
			return assignments

		var = self.MinRemainValue(csp, assignments)
		csp_tmp = deepcopy(csp)

		for value in csp.Domains[csp.Vars.index(var)]:
			inferences = {}
			
			if csp.consistency(var, value): 
				assignments[var] = value
				#print(assignments)
				inferences = self.ForwardChecking(csp, var, value, assignments)
				
				if isinstance(inferences, dict):
					assignments.update(inferences)
					#print(assignments)
					output = self.BackTracking(csp, assignments)
					if isinstance(output, dict): 

						return output
			del assignments[var]#if var in assignments.keys(): del assignments[var]

			if isinstance(inferences, dict):
				for infer in inferences:
					del assignments[infer]#if infer in assignments.keys(): del assignments[infer]
			csp = deepcopy(csp_tmp)

		return False


       
def main():
	# parse arguments
	ap = argparse.ArgumentParser()
	ap.add_argument("sudoku_string", type = str, help = "Input sudoku string.")
	#ap.add_argument("output.txt", type = str, help = "Output the result into a txt file.")
	args = ap.parse_args()

	# initialize csp
	csp_string = args.sudoku_string
	#print(type(csp_string), csp_string)
	csp = CSP(csp_string)
	#print csp.Domains

	Sudoku = SudokuSolver()
	if False: #Sudoku.AC3(csp) and csp.solvable(): 
		#print ("Sudoku solved with AC3!")
		#print (csp.Domains)
		output = csp.Output()
		print(output)
	else:
		assignments = Sudoku.BackTracking(csp, {})
		print(assignments)
		output = csp.Output()
		print(output)

	# output results
	f = open('output.txt', "w")
	f.write(output)
	f.close()
		
		
"""

	Sudoku = SudokuSolver()
	assignments = Sudoku.BackTracking(csp, {})


	if isinstance(assignments, dict):
		result = csp.assign(assignments)
		print(result)
	else:
		print "Can't be solved!"

"""
if __name__=='__main__':
	main()






