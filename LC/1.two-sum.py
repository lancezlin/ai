class Solution(object):
    def twoSum(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        maps = {}
        for i in range(len(nums)):
            if target-nums[i] in maps:
                return [maps[target-nums[i]], i]
            maps[nums[i]] = i
