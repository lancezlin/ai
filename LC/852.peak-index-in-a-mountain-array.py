class Solution(object):
    def peakIndexInMountainArray(self, A):
        """
        :type A: List[int]
        :rtype: int
        """
        n = len(A)
        left, right = 0, n-1
        while left+1<right:
            mid = left + (right-left)/2
            if self.is_peak(A, mid):
                return mid
            elif A[mid]>A[mid-1] and A[mid]<A[mid+1]:
                left = mid
            else:
                right = mid

    def is_peak(self, A, idx):
        return A[idx] > A[idx-1] and A[idx]>A[idx+1]
