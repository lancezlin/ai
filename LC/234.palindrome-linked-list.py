# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def isPalindrome(self, head):
        """
        :type head: ListNode
        :rtype: bool
        """
        if head is None or head.next is None:
            return True
        left, right = head, self.findMid(head)
        rev_right = self.reverseLS(right)
        while rev_right:
            if left != rev_right:
                return False
            left=left.next
            rev_right=rev_right.next
        return True

    def reverseLS(self, node):
        pre=ListNode(0)
        while node:
            tmp=node.next
            node.next=pre.next
            pre.next=node
            node=tmp
        return pre.next

    def findMid(self, node):
        slow, fast = node, node
        while fast.next and fast.next.next:
            slow=slow.next
            fast=fast.next.next
        return slow.next

