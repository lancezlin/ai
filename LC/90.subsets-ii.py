class Solution(object):
    def subsetsWithDup(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        nums.sort()
        res = [[]]
        start = 0
        for i in range(len(nums)):
            if i==0 or nums[i]!=nums[i-1]:
                start = 0
            size = len(res)
            for j in range(start, size):
                cur = list(res[j])
                cur.append(nums[i])
                res.append(cur)
            start = size

        return res
