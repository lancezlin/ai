class Solution(object):
    def minDistance(self, word1, word2):
        """
        :type word1: str
        :type word2: str
        :rtype: int
        """
        m, n = len(word1), len(word2)
        md = [[0]*(n+1) for i in range(m+1)]
        for i in range(m+1):
            for j in range(n+1):
                if i==0:
                    md[i][j] = j
                elif j==0:
                    md[i][j] = i
                elif word1[i-1]==word2[j-1]:
                    md[i][j] = md[i-1][j-1]
                else:
                    md[i][j] = 1+min(md[i-1][j], md[i-1][j-1], md[i][j-1])
        return md[m][n]
