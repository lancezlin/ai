"""
# Definition for a Node.
class Node(object):
    def __init__(self, val, children):
        self.val = val
        self.children = children
"""
class Solution(object):
    def preorder(self, root):
        """
        :type root: Node
        :rtype: List[int]
        """
        self.res = []
        self.pre(root)
        return self.res
    def pre(self, node):
        if node is None:
            return
        self.res.append(node.val)
        for child in node.children:
            self.pre(child)

