# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def detectCycle(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        if head is None or head.next is None:
            return None
        has_cycle=False
        slow, fast = head, head
        while fast.next and fast.next.next:
            slow=slow.next
            fast=fast.next.next
            if slow==fast:
                has_cycle=True
                slow=head
                break
        if has_cycle:
            while True:
                if slow==fast:
                    return fast
                slow=slow.next
                fast=fast.next
        return None


