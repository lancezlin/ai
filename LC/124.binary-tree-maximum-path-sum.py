# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def maxPathSum(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        _, maxsum = self.maxpath(root)
        return maxsum

    def maxpath(self, node):
        if node is None:
            return 0, float('-inf')
        from_left, in_left = self.maxpath(node.left)
        from_right, in_right = self.maxpath(node.right)
        max_from_node = max(0, from_left, from_right)+node.val
        max_in_node = max(in_left, in_right, max_from_node, from_left+from_right+node.val)
        return max_from_node, max_in_node
