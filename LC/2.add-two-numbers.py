# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def addTwoNumbers(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """
        if l1 is None:
            return l2
        if l2 is None:
            return l1
        add_one=0
        res=ListNode(0)
        dummy=res
        while True:
            if l1 is None and l2 is None:
                if add_one:
                    dummy.next = ListNode(1)
                    dummy=dummy.next
                break
            elif l1 and l2:
                sumup = l1.val+l2.val + add_one
                digit = sumup % 10
                add_one = sumup / 10
                dummy.next=ListNode(digit)
                dummy=dummy.next
                l1=l1.next
                l2=l2.next
            elif l1:
                sumup = l1.val + add_one
                digit = sumup % 10
                add_one = sumup / 10
                dummy.next=ListNode(digit)
                dummy=dummy.next
                l1=l1.next
            elif l2:
                sumup = l2.val + add_one
                digit = sumup % 10
                add_one = sumup / 10
                dummy.next=ListNode(digit)
                dummy=dummy.next
                l2=l2.next
        return res.next
