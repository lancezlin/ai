# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def swapPairs(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        if head is None or head.next is None:
            return head
        dummy = ListNode(0)
        dummy.next=head
        head=dummy
        while head.next and head.next.next:
            first, second = head.next, head.next.next
            first.next = second.next
            second.next = first
            head.next = second
            head = first
        return dummy.next
