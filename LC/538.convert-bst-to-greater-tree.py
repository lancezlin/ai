# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def convertBST(self, root):
        """
        :type root: TreeNode
        :rtype: TreeNode
        """
        self.sumup = 0
        self.dfs(root)
        return root
        
    def dfs(self, root):
        if root is None:
            return
        self.dfs(root.right)
        self.sumup += root.val
        root.val = self.sumup
        self.dfs(root.left)

