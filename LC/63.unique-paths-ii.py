class Solution(object):
    def uniquePathsWithObstacles(self, obstacleGrid):
        """
        :type obstacleGrid: List[List[int]]
        :rtype: int
        """
        if len(obstacleGrid)==0:
            return 0
        if obstacleGrid[0][0] == 1:
            return 0
        m, n = len(obstacleGrid), len(obstacleGrid[0])
        paths = [0]*n
        paths[0]=1
        for i in range(m):
            for j in range(n):
                if obstacleGrid[i][j] == 1:
                    paths[j] = 0
                elif j>0:
                    paths[j] = paths[j] + paths[j-1]
        return paths[n-1]
