# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def removeElements(self, head, val):
        """
        :type head: ListNode
        :type val: int
        :rtype: ListNode
        """
        if head is None:
            return None
        dummy=ListNode(0)
        dummy.next=head
        pre=dummy
        while head:
            while head and head.val==val:
                head=head.next
            pre.next=head
            if head is None:
                break
            head=head.next
            pre=pre.next
        return dummy.next
