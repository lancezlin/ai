class Solution(object):
    def findPeakElement(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if len(nums)==0:
            return 0
        if len(nums)==1:
            return 0
        left, right = 0, len(nums)-1
        while left+1<right:
            mid=left+(right-left)/2
            if nums[mid]>nums[mid+1] and nums[mid]>nums[mid-1]:
                return mid
            elif nums[mid]>nums[mid+1] and nums[mid]<nums[mid-1]:
                right = mid
            elif nums[mid]<nums[mid+1] and nums[mid]>nums[mid-1]:
                left=mid
            else:
                left=mid
        if nums[left]>nums[right]:
            return left
        return right
