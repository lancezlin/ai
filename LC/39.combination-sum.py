class Solution(object):
    def combinationSum(self, candidates, target):
        """
        :type candidates: List[int]
        :type target: int
        :rtype: List[List[int]]
        """
        res = []
        tmp = []
        if len(candidates)==0:
            return res
        # import pdb; pdb.set_trace()
        self.dfs(candidates, target, tmp, res)
        return res
        
    def dfs(self, candidates, target, tp, res):
        if target==0:
            if tp not in res:
                res.append(tp)
            return
        if target<0:
            return
        for candidate in candidates:
            tp.append(candidate)
            self.dfs(candidates, target-candidate, tp, res)

if __name__=="__main__":
    sol = Solution()
    print (sol.combinationSum([2,3,6,7], 7))
