"""
# Definition for a Node.
class Node(object):
    def __init__(self, val, children):
        self.val = val
        self.children = children
"""
class Solution(object):
    def postorder(self, root):
        """
        :type root: Node
        :rtype: List[int]
        """
        self.res = []
        self.post(root)
        return self.res
    def post(self, node):
        if node is None:
            return
        for child in node.children:
            self.post(child)
        self.res.append(node.val)
