class Solution(object):
    def coinChange(self, coins, amount):
        """
        :type coins: List[int]
        :type amount: int
        :rtype: int
        """
        cc = [-1]*(amount+1)
        cc[0] = 0
        for i in range(amount+1):
            if cc[i]<0:
                continue
            for c in coins:
                if i+c > amount:
                    continue
                elif (i+c)<=amount and cc[i+c]<0:
                    cc[i+c] = cc[i]+1
                else:
                    cc[i+c] = min(cc[i+c], cc[i]+1)
        return cc[amount]
