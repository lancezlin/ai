class Solution(object):
    def gameOfLife(self, board):
        """
        :type board: List[List[int]]
        :rtype: void Do not return anything, modify board in-place instead.
        """
        dx = [1, 1, 1, 0, 0, -1, -1, -1]
        dy = [1, 0, -1, 1, -1, 1, 0, -1]
        for x in range(len(board)):
            for y in range(len(board[0])):
                lives = 0
                for i in range(8):
                    nx, ny = x+dx[i], y+dy[i]
                    lives += self.getStatus(board, nx, ny)
                    if lives+board[x][y]==3 or lives==3:
                        board[x][y] |= 2
        for x in range(len(board)):
            for y in range(len(board[0])):
                board[x][y] >>= 1
        
    def getStatus(self, board, x, y):
        if x<0 or y<0 or x>len(board)-1 or y>len(board[0])-1:
            return 0
        else:
            return board[x][y]&1
