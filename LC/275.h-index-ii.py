class Solution(object):
    def hIndex(self, citations):
        """
        :type citations: List[int]
        :rtype: int
        """
        if len(citations) < 1:
            return 0
        N = len(citations)-1
        left, right = 0, N
        while left+1 < right:
            mid = left+(left+right)/2
            if citations[mid] == N+1-mid:
                return N+1-mid
            elif citations[mid] > N+1-mid:
                right = mid
            else:
                left = mid+1
        if citations[left] >= N+1-left:
            return N+1-left
        return N+1-right
