# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def preorderTraversal(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """
        self.res = []
        self.preorder(root)
        return self.res
        
    def preorder(self, node):
        if node is None:
            return
        self.res.append(node.val)
        self.preorder(node.left)
        self.preorder(node.right)

