class Solution(object):
    def searchRange(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        if len(nums)==0:
            return [-1,-1]
        left, right = 0, len(nums)-1
<<<<<<< HEAD
        while left<=right:
            mid = left+(right-left)/2
            if nums[mid]==target:
                start, end = mid, mid
                while start>left:
                    if nums[start-1]==target:
                        start-=1
                    else:
                        break
                while end<right:
                    if nums[end+1]==target:
                        end+=1
                    else:
                        break
                return [start, end]
            elif nums[mid] > target:
                right = mid-1
            else:
                left = mid+1
        return [-1, -1]
=======
        while left+1<right:
            mid=left+(right-left)/2
            if nums[mid]==target:
                left, right = mid, mid
                while left-1>=0 and nums[left-1]==target:
                    left-=1
                while right+1<len(nums) and nums[right+1]==target:
                    right+=1
                return [left, right]
            elif nums[mid]<target:
                left = mid
            else:
                right = mid
        if nums[left]!=target and nums[right]!=target:
            return [-1, -1]
        elif nums[left]==target and nums[right]==target:
            return [left, right]
        else:
            if nums[left]==target:
                return [left,left]
            else:
                return [right, right]
>>>>>>> 3966c33da6ab785a3e2b293d7d547e356b46c7e4

