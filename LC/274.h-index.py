class Solution(object):
    def hIndex(self, citations):
        """
        :type citations: List[int]
        :rtype: int
        """
        N = len(citations)
        counts = [0] * (N+1)
        for c in citations:
            if c >= N:
                counts[N] += 1
            else:
                counts[c] += 1
        sums = 0
        for h in range(N, 0, -1):
            if sums + counts[h] >= h:
                return h
            sums += counts[h]
        return 0
