class Solution(object):
    def containsDuplicate(self, nums):
        """
        :type nums: List[int]
        :rtype: bool
        """
        numset = set()
        for i in nums:
            if i not in numset:
                numset.add(i)
            else:
                return True
        return False
