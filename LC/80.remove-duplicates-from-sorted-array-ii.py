class Solution(object):
    def removeDuplicates(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if len(nums) <= 2:
            return len(nums)
        end = 2
        i = 2
        while i < len(nums):
            start = end - 2
            if nums[start] != nums[i]:
                nums[end] = nums[i]
                end += 1
            i += 1
        return end
