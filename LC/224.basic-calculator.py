class Solution(object):
    def calculate(self, s):
        """
        :type s: str
        :rtype: int
        """
        s = "(" + s + ")"
        k = 1
        return self.dfs(s, k)

    def dfs(self, s, i):
        res = 0; multi = 1; val = 0
        while i < len(s):
            if s[i].isdigit():
                val = val*10 + int(s[i])
            elif s[i]=="+" or s[i]=="-":
                res += (multi*val)
                val = 0
                if s[i]=="+":
                    multi = 1
                else:
                    multi = -1
            elif s[i]=="(":
                val = self.dfs(s, i+1)
            elif s[i]==")":
                res += (multi*val)
                return res
            i += 1
        return res
