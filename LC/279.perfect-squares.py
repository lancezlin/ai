class Solution(object):
    def numSquares(self, n):
        """
        :type n: int
        :rtype: int
        """
        ns = [float('Inf')]*(n+1)
        ns[0]=1
        i=1
        while i*i<(n+1):
            ns[i*i]=1
            i+=1
        for x in range(1, n+1):
            y=1
            while x+y*y<(n+1):
                ns[x+y*y]=min(ns[x]+1, ns[x+y*y])
                y+=1
        return ns[-1]
