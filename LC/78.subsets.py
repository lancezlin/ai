class Solution(object):
    def subsets(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        res = [[]]
        for i, num in enumerate(nums):
            m = len(res)
            for j in range(m):
                cur = list(res[j])
                cur.append(num)
                res.append(cur)
        return res
