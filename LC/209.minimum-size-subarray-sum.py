class Solution(object):
    def minSubArrayLen(self, s, nums):
        """
        :type s: int
        :type nums: List[int]
        :rtype: int
        """
        if len(nums)==0:
            return 0
        if sum(nums)<s:
            return 0
        min_len = float('Inf')
        subsum=0
        i, j = 0, 0
        while True:
            if subsum >= s:
                if i>j:
                    break
                min_len=min(min_len, j-i)
                subsum-=nums[i]
                i+=1
            else:
                if j>=len(nums):
                    break
                subsum+=nums[j]
                j+=1
        return min_len
