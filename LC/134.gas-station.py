class Solution(object):
    def canCompleteCircuit(self, gas, cost):
        """
        :type gas: List[int]
        :type cost: List[int]
        :rtype: int
        """
        start = 0
        total_gas = 0
        sum_gas = 0
        i = 0
        while i<len(gas):
            total_gas += gas[i] - cost[i]
            sum_gas += gas[i] - cost[i]
            if sum_gas<0:
                start = i + 1
                sum_gas = 0
            i += 1
        if total_gas < 0:
            return -1
        return start
