class Solution(object):
    def wordBreak(self, s, wordDict):
        """
        :type s: str
        :type wordDict: List[str]
        :rtype: bool
        """
        wb=[False]*(len(s)+1)
        wb[0]=True
        for i in range(len(s)+1):
            for k in range(i):
                if wb[k] and s[k:i] in wordDict:
                    wb[i]=True
        return wb[-1]
