class Solution(object):
    def rotate(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: void Do not return anything, modify nums in-place instead.
        """
        # method 1
        """
        k = k % len(nums)
        if k == 0:
            return nums
        while k > 0:
            last_pos = len(nums) - 1
            tmp = nums[last_pos]
            while last_pos > 0:
                nums[last_pos] = nums[last_pos-1]
                last_pos -= 1
            nums[last_pos] = tmp
        """
        # method 2
        k = k % len(nums)
        if k == 0:
            return
        # reverse itself
        self.reverse(nums, 0, len(nums)-1)
        self.reverse(nums, 0, k-1)
        self.reverse(nums, k, len(nums)-1)
    
    def reverse(self, nums, i, j):
        while i < j:
            tmp = nums[i]
            nums[i] = nums[j]
            nums[j] = tmp
            i += 1
            j -= 1
