class Solution(object):
    def myAtoi(self, str):
        """
        :type str: str
        :rtype: int
        """
        if len(str)==0:
            return 0
        has_minus=False
        has_plus=False
        i = 0; N = len(str)
        x=0
        while i<N and str[i]==" ":
            i+=1
        if i<N and str[i] == "-":
            has_minus=True
            is_first=False
            i+=1
        elif i<N and str[i] == "+":
            has_plus = True
            is_first=False
            i+=1
        while i<N:
            if str[i].isdigit():
                x = 10*x+int(str[i])
                i+=1
            else:
                break

        if has_minus:
            x=-x
        if x > 2**31-1:
            return 2**31-1
        if x < -2**31:
            return -2**31
        return x
