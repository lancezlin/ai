class Solution(object):
    def countPrimes(self, n):
        """
        :type n: int
        :rtype: int
        """
        if n<2:
            return 0
        primes = [True]*(n-1)
        primes[0] = False
        for i in range(2, int(n**0.5)+1):
            if primes[i-1]:
                for j in range(i*i, n, i):
                    primes[j-1] = False
        res = 0
        for i in range(n-1):
            if primes[i]:
                res+=1
        return res
