class Solution(object):
    def letterCombinations(self, digits):
        """
        :type digits: str
        :rtype: List[str]
        """
        self.maps = {2: ['a', 'b', 'c'],
                3: ['d', 'e', 'f'],
                4: ['g', 'h', 'i'],
                5: ['j', 'k', 'l'],
                6: ['m', 'n', 'o'],
                7: ['p', 'q', 'r', 's'],
                8: ['t', 'u', 'v'],
                9: ['w', 'x', 'y', 'z']}
        self.res = []
        if len(digits)==0:
            return self.res
        tmp = ""
        self.dfs(digits, 0, self.maps, tmp)
        return self.res

    def dfs(self, digits, start, maps, tmp):
        if len(tmp) == len(digits):
            if tmp not in self.res:
                self.res.append(tmp)
            return
        for mp in maps[int(digits[start])]:
            self.dfs(digits, start+1, maps, tmp+str(mp))
         

