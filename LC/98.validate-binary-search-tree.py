# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def isValidBST(self, root):
        """
        :type root: TreeNode
        :rtype: bool
        """
        if root is None:
            return True
        return self.dfs(root, float('-Inf'), float('Inf'))

    def dfs(self, root, low, high):

        if root is None:
            return True
        if root.val >= high or root.val<=low:
            return False
        return self.dfs(root.left, low, root.val) and self.dfs(root.right, root.val, high)
