# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def rotateRight(self, head, k):
        """
        :type head: ListNode
        :type k: int
        :rtype: ListNode
        """
        if head is None or head.next is None:
            return head
        N=1
        head_copy=head
        while head.next:
            N+=1
            head=head.next
        head.next=head_copy
        if N < k:
            k = k % N
        res_k = N-k
        ahead=head.next
        while res_k>0:
            head=head.next
            ahead=ahead.next
            res_k-=1
        head.next=None
        return ahead
