class Solution(object):
    def nthUglyNumber(self, n):
        """
        :type n: int
        :rtype: int
        """
        ugly = [0] * n
        ugly[0] = 1
        i2, i3, i5 =0, 0, 0
        for i in range(1, n):
            m2, m3, m5 = ugly[i2]*2, ugly[i3]*3, ugly[i5]*5
            ugly[i] = min(m2, m3, m5)
            if ugly[i] == m2:
                i2 += 1
            if ugly[i] == m3:
                i3 += 1
            if ugly[i] == m5:
                i5 += 1
        return ugly[-1]
