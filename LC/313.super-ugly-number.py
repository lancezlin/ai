class Solution(object):
    def nthSuperUglyNumber(self, n, primes):
        """
        :type n: int
        :type primes: List[int]
        :rtype: int
        """
        ugly = [0] * n
        idx = [0] * len(primes)
        ugly[0] = 1
        for i in range(1, n):
            next_vals = [ugly[idx[j]]*primes[j] for j in range(len(primes))]
            ugly[i] = min(next_vals)
            for j in range(len(primes)):
                if ugly[i] == ugly[idx[j]]*primes[j]:
                    idx[j] += 1
        return ugly[-1]
