class Solution(object):
    def missingNumber(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        n = len(nums)
        sumOfNums = n*(n+1)/2
        return sumOfNums - sum(nums)
