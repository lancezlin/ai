class Solution(object):
    def searchInsert(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        left, right=0, len(nums)-1
        while left+1<right:
            mid=left+(right-left)/2
            if nums[mid]==target:
                return mid
            elif nums[mid]>target:
                right=mid
            else:
                left=mid
        if nums[left]>=target:
            return 0
        if nums[right]==target:
            return right
        if nums[right]<target:
            return right+1
        return right
