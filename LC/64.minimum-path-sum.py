class Solution(object):
    def minPathSum(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        m, n = len(grid), len(grid[0])
        if m==1 and n==1:
            return grid[0][0]
        mps = [0]*n # [[0]*n for i in range(m)]
        mps[0] = grid[0][0]
        for i in range(m):
            for j in range(n):
                if i==0 and j>0:
                    mps[j]=mps[j-1]+grid[i][j]
                    continue
                elif i>0 and j==0:
                    mps[j] = mps[j]+grid[i][j]
                else:
                    mps[j] = min(mps[j-1]+grid[i][j], mps[j]+grid[i][j])
        return mps[-1]

