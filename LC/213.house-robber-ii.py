class Solution(object):
    def rob(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if len(nums)==0:
            return 0
        if len(nums)<3:
            return max(nums)
        return max(self._rob(nums[:-1]), self._rob(nums[1:]))

    def _rob(self, num):
        pre, curMax = 0, 0
        for x in num:
            tmp = curMax
            curMax = max(pre+x, curMax)
            pre = tmp
        return curMax
