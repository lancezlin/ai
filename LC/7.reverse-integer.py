class Solution(object):
    def reverse(self, x):
        """
        :type x: int
        :rtype: int
        """
        is_neg = False
        if x<0:
            is_neg=True
            x=-x
        y=0
        while x>0:
            digit = x%10
            y=y*10+digit
            x=x/10
            if y > 2**31-1:
                return 0
        if is_neg:
            return -1*y
        return y
