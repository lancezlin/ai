def f(d, n):
    pads={0:["8"],
            1:["2", "4"],
            2:["1","3", "5"],
            3:["2","6"],
            4:["1","5","7"],
            5:["2","4","6","8"],
            6:["3","5","9"],
            7:["4","8"],
            8:["0","5","7","9"],
            9:["6","8"]
        }
    res = []
    if n==0:
        return res
    dfs(d, str(d), n, res, pads)
    return res

def dfs(start, output, nums, res, pads):
    if len(output)==nums:
        if int(output) not in res:
            res.append(int(output))
        return
    for num_str in pads[start]:
        num = int(num_str)
        dfs(num, output+num_str, nums, res, pads)

if __name__=="__main__":
    print ('f(8, 3): {}'.format(f(8, 3)))
    print(f(6,1))
    print (f(1, 2))
    print(f(1,3))
    print(f(0,3))
    print(f(0,0))
