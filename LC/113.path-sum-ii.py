# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def pathSum(self, root, sum):
        """
        :type root: TreeNode
        :type sum: int
        :rtype: List[List[int]]
        """
        if root is None:
            return []
        self.res = []
        self.helper(root, sum, [])
        return self.res
        
    def helper(self, root, sum, output):
        if root.left is None and root.right is None:
            if root.val == sum:
                output.append(root.val)
                self.res.append(output)
        if root.left:
            left = self.helper(root.left, sum-root.val, output+[root.val])
        if root.right:
            right = self.helper(root.right, sum-root.val, output+[root.val])

