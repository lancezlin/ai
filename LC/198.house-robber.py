class Solution(object):
    def rob(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if nums is None or len(nums)==0:
            return 0
        """
        dp = [0]*(N+1)
        dp[1] = nums[0]
        for i in range(1, N):
            j = i+1
            dp[j] = max(dp[j-1], dp[j-2]+nums[i])
        return dp[-1]
        """
        prev, cur = 0, 0
        for x in nums:
            tmp = cur
            cur = max(prev+x, cur)
            prev = tmp
        return cur
