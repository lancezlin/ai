# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def mergeKLists(self, lists):
        """
        :type lists: List[ListNode]
        :rtype: ListNode
        """
        if not lists:
            return None
        K=len(lists)-1
        while K>0:
            i=0
            while i<K:
                lists[i]=self.merge2Lists(lists[i], lists[K])
                i+=1
                K-=1
        return lists[0]
        
    def merge2Lists(self, l1, l2):
        dummy=ListNode(0)
        pre=dummy
        while l1 or l2:
            if l1 and l2:
                if l1.val > l2.val:
                    pre.next=l2
                    l2=l2.next
                else:
                    pre.next = l1
                    l1=l1.next
                pre=pre.next
            elif l1:
                pre.next=l1
                break
            else:
                pre.next=l2
                break
        return dummy.next
