class Solution(object):
    def findKthLargest(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: int
        """
        hp = []
        import heapq
        for num in nums:
            heapq.heappush(hp, -num)
        for i in range(k):
            res=heapq.heappop(hp)
        return -res
