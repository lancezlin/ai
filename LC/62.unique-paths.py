class Solution(object):
    def uniquePaths(self, m, n):
        """
        :type m: int
        :type n: int
        :rtype: int
        """
        paths = [[0]*n for i in range(m)]
        for i in range(0, m):
            for j in range(0, n):
                if i == 0 or j==0:
                    paths[i][j] = 1
                else:
                    paths[i][j] = paths[i-1][j]+paths[i][j-1]
        return paths[m-1][n-1]
