# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def mergeTwoLists(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """
        if l1 is None and l2 is None:
            return None
        if l1 is None:
            return l2
        if l2 is None:
            return l1
        newl=ListNode(0)
        dummy=newl
        while l1 or l2:
            if l1 and l2:
                if l1.val>l2.val:
                    newl.next=l2
                    l2=l2.next
                    newl=newl.next
                else:
                    newl.next=l1
                    l1=l1.next
                    newl=newl.next
            elif l1 and not l2:
                newl.next=l1
                return dummy.next
            elif l2 and not l1:
                newl.next=l2
                return dummy.next

