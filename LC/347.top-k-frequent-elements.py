class Solution(object):
    def topKFrequent(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: List[int]
        """
        import heapq
        maps={}
        hq=[]
        res=[]
        for nu in nums:
            if nu in maps:
                maps[nu]+=1
            else:
                maps[nu]=1
        for key in maps:
            heapq.heappush(hq, (-maps[key], key))
        for i in range(k):
            res.append(heapq.heappop(hq)[1])
        return res

