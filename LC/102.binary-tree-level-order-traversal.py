# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def levelOrder(self, root):
        """
        :type root: TreeNode
        :rtype: List[List[int]]
        """
        res = []
        if root is None:
            return []
        queue = [root]
        while queue:
            sub_res = []
            num_items = len(queue)
            for i in range(num_items):
                front = queue.pop(0)
                sub_res.append(front.val)
                if front.left:
                    queue.append(front.left)
                if front.right:
                    queue.append(front.right)
            res.append(sub_res)
        return res
