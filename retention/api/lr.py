#! /usr/bin/python

# import machine learning libraries
import sys
import os
import pandas as pd
import numpy as np
#import matplotlib.pyplot as plt
from sklearn.preprocessing import Imputer
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn.cross_validation import train_test_split
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import ShuffleSplit
from sklearn.metrics import roc_auc_score
import yaml

# -------------------------------- Configurations ------------------------ #
# loading configurations
path_to_config = "/home/lancel/Projects/ai/retention/api/config.yaml"

with open(path_to_config, 'r') as f:
    try:
        ml_conf = yaml.load(f)
        print (ml_conf)
    except yaml.YAMLError as exc:
        print("Error loading configuration file - {}".format(exc))

# read the data from disk
path_to_data = os.path.join(ml_conf['path_to_data'], "data.csv")
print("path to data - {}".format(path_to_data))
dataset = pd.read_csv(path_to_data)
# dataset = pd.read_csv(path_to_data, index_col=0)
dataset.head(2)

# -------------------------------- Preprocessing ------------------------ #
# extract the data from dataframe
label_col = ml_conf['label_col']
numeric_cols = ml_conf['input_cols']['numeric_cols']
binary_cols = ml_conf['input_cols']['binary_cols']
numeric_cols.extend(binary_cols)
binary_index = numeric_cols.index(binary_cols[0])
print("The binary col: {0} and index: {1} and nums_cols: {2}".format(binary_cols[0], \
    binary_index, numeric_cols))


X = dataset[numeric_cols].values
y = dataset[label_col].values

# encoder the label
labelencoder_y = LabelEncoder()
y = labelencoder_y.fit_transform(y)

# encoder the binary label
labelencoder_X = LabelEncoder()
X[:, binary_index] = labelencoder_X.fit_transform(X[:, binary_index])

# train test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = ml_conf['test_split_percent'], random_state = 0)

# scaling the model
sc_X = MinMaxScaler()
X_train = sc_X.fit_transform(X_train)
X_test = sc_X.transform(X_test)

# ------------------------------- Modeling ------------------------ #
# ready for model

# training a DescisionTreeClassifier
dtree_model = DecisionTreeClassifier(max_depth = 2).fit(X_train, y_train)
dtree_pred = dtree_model.predict(X_test)
# creating a confusion matrix
cm = confusion_matrix(y_test, dtree_pred)
# feature importance - DT
print ("Feature importance of decision tree model - {}".format(dtree_model.feature_importances_))
print ("The confusion matrix of DT is {}".format(cm))
print ("The accuracy of DT model is {}".format(accuracy_score(y_test, dtree_pred)))


# training Support Vector Machine Classifier
svm_model = SVC(kernel = 'linear', C = 1, probability = True).fit(X_train, y_train)
svm_pred = svm_model.predict(X_test)
# model accuracy for X_test
svm_acc = accuracy_score(y_test, svm_pred)
# creating a confusion matrix
cm = confusion_matrix(y_test, svm_pred)
print ("The confusion matrix of SVM is {}".format(cm))
print ("The accuracy of SVM model is {}".format(svm_acc))

# ------------------------------- Evaluations ------------------------ #
# shuffle and cross validation
cv = ShuffleSplit(n_splits=5, test_size=0.3, random_state=42)
# 1. decision tree
dt_cv_score = cross_val_score(dtree_model, X_train, y_train, cv=cv)
print ("CV score of decision tree: {0}".format(dt_cv_score))

# 2. svm model
svm_cv_score = cross_val_score(svm_model, X_train, y_train, cv=cv)
print ("CV score of SVM: {0}".format(svm_cv_score))



# ------------------------------- Model persistency ------------------------ #





# ------------------------------- Model Visualization ------------------------ #
# plot decision tree structure
from sklearn.externals.six import StringIO
from IPython.display import Image
from sklearn.tree import export_graphviz
import pydotplus

dot_data = StringIO()
export_graphviz(dtree_model, 
        out_file=dot_data,
        filled = True,
        rounded = True,
        special_characters = True)
graph = pydotplus.graph_from_dot_data(dot_data.getvalue())
Image(graph.create_png())

# save to local 
# (comment out below line)
# graph.write_png('decision_tree_model.png')




