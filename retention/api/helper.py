import pickle
import os
import logging
import numpy as np
import sklearn
import json
import keras.models import model_from_json, load_model
import yaml
from staticconf.loader import yaml_loader

# save model to local
def save_model_to_local(name, model, model_dir='../models'):
    """
    """
    serialized_model = pickle.dumps(model, protocol=pickle.HIGHEST_PROTOCOL)
    model_path = os.path.join(model_dir, name+'.model')
    open(model_path, 'wb').write(serialized_model)


# load model from local
def load_model_from_local(name, model_dir='../models'):
    """"""
    model_path = os.path.join(model_dir, name+'.model')
    try:
        model = pickle.loads(open(model_path, 'rb').read())
    except Exception as e:
        print (e)
        return None
    return model

# add logger object 
def get_logger(log_name, log_level=logging.INFO, console_log_level=logging.CRITICAL):
    """"""
    logger = logging.getLogger(log_name)
    logger.setLevel(log_level)
    try:
        f = os.open(log_name, os.O_CREAT|os.O_EXCL|os.O_WRONLY, 0666)
        os.close(f)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise
    handler = logging.handlers.RotatingFileHandler(log_name, \
            maxBytes = 200000000,
            backupCount = 10)
    handler.setLevel(log_level)
    formatter = logging.Formatter(
      '%(asctime)s-%(filename)s:%(lineno)s-%(funcName)s-%(levelname)s-%(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    console = logging.StreamHandler()
    console.setLevel(console_log_level)
    console.setFormatter(formatter)
    logger.addHandler(console)
    return logger



